<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InfoappSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Infoapps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infoapp-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Infoapp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'dowload',
            'dia',
            'mes',
            'ano',
            //'login',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
