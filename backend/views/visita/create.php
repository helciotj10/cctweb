<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Visita */

$this->title = 'Adicionar Midia de Visita Guiada';
//$this->params['breadcrumbs'][] = ['label' => 'Visitas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visita-create">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="header">
							<h2 class="title"><?= Html::encode($this->title) ?></h2>
							<br>
							<ol class="breadcrumb" >
								<li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
								<li><a href="index.php?r=visita"><i class=""></i> Visita Guiada</a></li>
								<li class="active">Adicionar Midia de Visita</li>
							</ol>
						</div>
						<div class="content">
							<?= $this->render('_form', [
								'model' => $model,
								]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
