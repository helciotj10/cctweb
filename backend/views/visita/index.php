<?php

use yii\helpers\Html;
use yii\grid\GridView;
use cics\widgets\VideoEmbed;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VisitaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Midias 360';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visita-index">

    <div class="panel panel-default" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); padding: 10px; padding-top: 20px; padding-bottom: 20px;">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Adicionar Videos ou Fotos', ['create'], ['class' => 'btn btn-success']) ?>
    <br>
    </div>
    <div class="row">
    <?php  foreach ($dataProvider->getModels() as $m) {  if ($m->foto === NULL) { ?>
            <div class="col-lg-4 col-sm-6">
                <div class="card" >
                    <div class="content">
                        <div class="row" style="margin-left: 1px;">
                            <iframe src="https://www.youtube.com/embed/<?=$m->video?>?loop=1&modestbranding=1" width="300" height="200" frameborder="0" allowfullscreen=""></iframe>
                        </div>
                        <div class="footer">
                            <hr />
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="stats">
                                        <i class="ti-image"></i> <?= $m->descricao ?>
                                    </div>
                                </div>
                            
                                <div class="col-lg-5">
                                     <?php echo "</td><td><p style='padding-top: 10px;'><a  href='?r=visita/view&id=".$m->id."' title='Ver detalhes' data-pjax='0'>
                                        <i class='fa fa-search-plus'>
                                        </i>
                                      </a><a href='?r=visita/update&id=".$m->id."' title='Actualizar' data-pjax='0'>
                                    <i class='glyphicon glyphicon-pencil'>
                                    </i>
                                    </a>
                                    <a href='?r=visita/delete&id=".$m->id."' title='Eliminar' data-pjax='0' data-confirm='Deseja realmente eliminar esta Midia?' data-method='post'>
                                    <i class='glyphicon glyphicon-trash'>
                                    </i>
                                    </a></p></td></tr>"; ?>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }} ?>
        </div>
        <div class="row">
          <?php  foreach ($dataProvider->getModels() as $m) { if ($m->foto != NULL) {  ?>
            <div class="col-lg-4 col-sm-6">
                <div class="card" style="width: 330px; height: 300px;">
                    <div class="content">
                        <div class="row" style="margin-left: 1px;">
                            <img style="width: 300px; height: 200px;" class="img-responsive"  src="uploads<?=$m->foto?>" alt="..."/> 
                        </div>
                        <div class="footer row">
                            <hr />
                            
                                <div class="col-lg-8">
                                    <div class="stats">
                                        <i class="ti-image"></i> <?= $m->descricao?>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                     <?php echo "</td><td><p style='padding-top: 10px;'><a  href='?r=visita/view&id=".$m->id."' title='Ver detalhes' data-pjax='0'>
                                        <i class='fa fa-search-plus'>
                                        </i>
                                      </a><a href='?r=visita/update&id=".$m->id."' title='Actualizar' data-pjax='0'>
                                    <i class='glyphicon glyphicon-pencil'>
                                    </i>
                                    </a>
                                    <a href='?r=visita/delete&id=".$m->id."' title='Eliminar' data-pjax='0' data-confirm='Deseja realmente eliminar esta Midia?' data-method='post'>
                                    <i class='glyphicon glyphicon-trash'>
                                    </i>
                                    </a></p></td></tr>"; ?>
                                </div>  
                            
                        </div>
                    </div>
                </div>
            </div>
        
    <?php }} ?>
    </div>

</div>
