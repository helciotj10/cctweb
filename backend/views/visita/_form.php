<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Visita */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visita-form">

	
   <?php $form = ActiveForm::begin(); ?>
   <form>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= $form->field($model, 'video')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
             <?= $form->field($model, 'fotoc')->widget(FileInput::classname(), [
                'name' => 'attachment_48[]',
                'options'=>[
                    'multiple'=>true
                ],
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/uploads']),
                    'uploadExtraData' => [
                        'album_id' => 20,
                        'cat_id' => 'Nature'
                    ],
                    'maxFileCount' => 10
                ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
               <?= $form->field($model, 'descricao')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
           </div>
       </div>
   </div>

   <div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'estado')->dropDownList(['1' => 'Ativo', '0' => 'Inativo']) ?>
        </div>
    </div>
</div>

<div class="text-center">
    <?= Html::submitButton($model->isNewRecord ? 'Quardar Midia' : 'Salvar Atualizações', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
</div>

<div class="clearfix"></div>
</form>
<?php ActiveForm::end(); ?>

</div>
