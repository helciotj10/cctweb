<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Visita */

$this->title = 'Detalhes da Midia';
$this->params['breadcrumbs'][] = ['label' => 'Visitas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visita-view">
    <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h2 class="title"><?= Html::encode($this->title)?></h2>
                                <br>
                                <ol class="breadcrumb" >
                                    <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                                    <li><a href="index.php?r=visita"><i class=""></i> Visita Guiada</a></li>
                                    <li class="active">Detalhes da Midia</li>
                                </ol>
                            </div>
                            <div class="content">
                                <form>
                                   <?php if ($model->foto === NULL){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Codigo do Video</label>
                                                <p class="form-control border-input"> <?=$model->video?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Video</label>
                                                <div class="delemita">
                                                <iframe src="https://www.youtube.com/embed/<?=$model->video?>?loop=1&modestbranding=1" width="560" height="315" frameborder="0" allowfullscreen=""></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($model->foto != NULL) {  ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                <label>Foto</label>
                                                <div class="delemita">
                                                    <img style="width: 50%; height: 50%;" class="img-responsive"  src="uploads<?=$model->foto?>" alt="..."/> 
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descrição</label>
                                                <p class="form-control border-input"> <?=$model->descricao?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Estado</label>
                                                <p class="form-control border-input"> <?=$model->estado?> </p>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="text-center">
                                            <?= Html::a('Atualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                            <?= Html::a('Apagar', ['delete', 'id' => $model->id], [
                                                'class' => 'btn btn-danger',
                                                'data' => [
                                                    'confirm' => 'Are you sure you want to delete this item?',
                                                    'method' => 'post',
                                                ],
                                            ]) ?>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
