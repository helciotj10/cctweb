<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MidiaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="midia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idMidia') ?>

    <?= $form->field($model, 'video') ?>

    <?= $form->field($model, 'foto') ?>

    <?= $form->field($model, 'descricaoPT') ?>

    <?= $form->field($model, 'descricaoEN') ?>

    <?php // echo $form->field($model, 'descricaoFR') ?>

    <?php // echo $form->field($model, 'descricaoES') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'idHistoria') ?>

    <?php // echo $form->field($model, 'idTestemunho') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
