<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Midia */

$this->title = 'Detalhes da Midia';
$this->params['breadcrumbs'][] = ['label' => 'Midias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="midia-view">

     <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h2 class="title"><?= Html::encode($this->title)?></h2>
                                <br>
                                <?php if ($model->idHistoria != NULL){ ?>
                                    <ol class="breadcrumb" >
                                        <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                                        <li><a href="index.php?r=historia%2Fview&id=1"><i class=""></i> Historia</a></li>
                                        <li class="active">Detalhes da Midia</li>
                                    </ol>
                                <?php } elseif ($model->idTestemunho != NULL) {  ?>
                                    <ol class="breadcrumb" >
                                        <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                                        <li><a href="index.php?r=testemunho%2Fview&id=<?=$model->idTestemunho?>"><i class=""></i> Testimunho</a></li>
                                        <li class="active"> Detalhes da Midia</li>
                                    </ol>
                                <?php } else { ?>
                                    <ol class="breadcrumb" >
                                        <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                                        <li><a href="index.php?r=midia"><i class=""></i> Galeria</a></li>
                                        <li class="active"> Detalhes da Midia</li>
                                    </ol>
                                <?php } ?>

                            </div>
                            <div class="content">
                                <form>
                                   <?php if ($model->foto === NULL){ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Codigo do Video</label>
                                                <p class="form-control border-input"> <?=$model->video?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Video</label>
                                                <div class="delemita">
                                                <iframe src="https://www.youtube.com/embed/<?=$model->video?>?loop=1&modestbranding=1" width="560" height="315" frameborder="0" allowfullscreen=""></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if ($model->foto != NULL) {  ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                <label>Foto</label>
                                                <div class="delemita">
                                                    <img style="width: 50%; height: 50%;" class="img-responsive"  src="uploads<?=$model->foto?>" alt="..."/> 
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descrição</label>
                                                <p class="form-control border-input"> <?=$model->descricaoPT?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Estado</label>
                                                <p class="form-control border-input"> <?=$model->estado?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tipo</label>
                                                <p class="form-control border-input"> <?=$model->tipo?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Historia</label>
                                                <p class="form-control border-input"> <?=$model->idHistoria?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Testemunho</label>
                                                <p class="form-control border-input"> <?=$model->idTestemunho?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Pontos de Interesse</label>
                                                <p class="form-control border-input"> <?=$model->idPontos?> </p>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="text-center">
                                            <?= Html::a('Atualizar', ['update', 'id' => $model->idMidia], ['class' => 'btn btn-primary']) ?>
                                           
                                            <?= Html::a('Apagar', ['delete', 'id' => $model->idMidia], [
                                                'class' => 'btn btn-danger',
                                                'data' => [
                                                    'confirm' => 'Deseja realmente eliminar esta Midia?',
                                                    'method' => 'post',
                                                ],
                                            ])?>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
