<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use common\models\Historia;
use common\models\Testemunho;
use common\models\Pontosinterece;
use yii\helpers\ArrayHelper;

if (Html::encode($this->title) == 'Adicionar Midia') {
    if($testemunho != NULL){$model->idTestemunho = $testemunho;}
    if($historia != NULL){$model->idHistoria = $historia;}
    if($pontos != NULL){$model->idPontos = $pontos;}

}


/* @var $this yii\web\View */
/* @var $model common\models\Midia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="midia-form">

    
   <?php $form = ActiveForm::begin(); ?>
   <form>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= $form->field($model, 'video')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
             <?= $form->field($model, 'fotoc')->widget(FileInput::classname(), [
                'name' => 'attachment_48[]',
                'options'=>[
                    'multiple'=>true
                ],
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/uploads']),
                    'uploadExtraData' => [
                        'album_id' => 20,
                        'cat_id' => 'Nature'
                    ],
                    'maxFileCount' => 10
                ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
               <?= $form->field($model, 'descricaoPT')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
           </div>
       </div>
   </div>

   <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <?= $form->field($model, 'estado')->dropDownList(['1' => 'Ativo', '0' => 'Inativo'], ['class' => 'form-control border-input']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= $form->field($model, 'tipo')->dropDownList(['1' => 'Foto do Campo', '2' => 'Foto de Cela', '3' => 'Foto de Espólio'], ['prompt'=>'Selecione uma opção','class' => 'form-control border-input']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'idHistoria')->dropDownList(
                ArrayHelper::map(Historia::find()->all(), 'idHistoria','tituloPT'),
                ['prompt'=>'--','class' => 'form-control border-input']
                )?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               <?= $form->field($model, 'idTestemunho')->dropDownList(
                ArrayHelper::map(Testemunho::find()->all(),'id','nome'),
                ['prompt'=>'--','class' => 'form-control border-input']
                )?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               <?= $form->field($model, 'idPontos')->dropDownList(
                ArrayHelper::map(Pontosinterece::find()->all(),'id','nome'),
                ['prompt'=>'--','class' => 'form-control border-input']
                )?>
            </div>
        </div>
    </div>
    
    <div class="text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar Midia' : 'Salvar Atualizações', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<div class="clearfix"></div>
</form>
<?php ActiveForm::end(); ?>

</div>
