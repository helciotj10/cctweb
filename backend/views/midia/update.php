<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Midia */

$this->title = 'Atualizar Midia';
//$this->params['breadcrumbs'][] = ['label' => 'Midias', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->idMidia, 'url' => ['view', 'id' => $model->idMidia]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="midia-update">

	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="header">
							<h2 class="title"><?= Html::encode($this->title) ?></h2>
							<br>
							<ol class="breadcrumb" >
								<li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
								<li><a href="index.php?r=midia"><i class=""></i> Galeria</a></li>
								<li class="active"> Adicionar Midia</li>
							</ol>
						</div>
						<div class="content">

							<?= $this->render('_form', [
								'model' => $model,
								]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
