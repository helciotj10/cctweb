<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DoacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Funcionalidade Disponivel em  Breve';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doacao-index">

    <div class="panel panel-default" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); padding: 10px; padding-top: 1px; padding-bottom: 1px;">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <p>
        <?php // Html::a('Create Doacao', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php /* GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idDoacao',
            'nome',
            'valor',
            'data',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */?>
</div>
