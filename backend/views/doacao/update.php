<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Doacao */

$this->title = 'Update Doacao: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Doacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idDoacao, 'url' => ['view', 'id' => $model->idDoacao]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="doacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
