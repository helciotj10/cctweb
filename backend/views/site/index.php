<?php

/* @var $this yii\web\View */
use scotthuangzl\googlechart\GoogleChart;

$this->title = 'CCT';
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-warning text-center">
                                <i class="ti-import"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <p>Downloads Realizados</p>
                                <?= $infoapp::find()->where(['dowload' => 1])->count(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr />
                        <div class="stats">
                            <i class="ti-reload"></i> Ultima Atualização
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-success text-center">
                                <i class="ti-user"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <p>Utilizadores Registrados</p>
                                <?= $usuario::find()->where(['status' => 1])->count(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr />
                        <div class="stats">
                            <i class="ti-reload"></i> Ultima Atualização
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-danger text-center">
                                <i class="ti-user"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <p>Utilizadores Desativados</p>
                                <?= $usuario::find()->where(['status' => 0])->count(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr />
                        <div class="stats">
                            <i class="ti-reload"></i> Ultima Atualização
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-success text-center">
                                <i class="ti-video-camera"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <p>Depoimentos Inseridos</p>
                                <?= $testemunho::find()->count(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr />
                        <div class="stats">
                            <i class="ti-reload"></i> Ultima Atualização
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-success text-center">
                                <i class="ti-image"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <p>Midias Inseridas</p>
                                <?= $midia::find()->count(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr />
                        <div class="stats">
                            <i class="ti-reload"></i> Ultima Atualização
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-success text-center">
                                <i class="ti-direction"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <p>Pontos Inseridos</p>
                                <?= $pontosinterece::find()->count(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr />
                        <div class="stats">
                            <i class="ti-reload"></i> Ultima Atualização
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Controlo de Acessos</h4>
                    <p class="category">Numero de login para 
                     <?php if ((date('m')-1) == 1) {echo "Janeiro";}?>
                     <?php if ((date('m')-1) == 2) {echo "Fevereiro";}?>
                     <?php if ((date('m')-1) == 3) {echo "Março";}?>
                     <?php if ((date('m')-1) == 4) {echo "Abril";}?>
                     <?php if ((date('m')-1) == 5) {echo "Maio";}?>
                     <?php if ((date('m')-1) == 6) {echo "Junho";}?>
                     <?php if ((date('m')-1) == 7) {echo "Julho";}?>
                     <?php if ((date('m')-1) == 8) {echo "Agosto";}?>
                     <?php if ((date('m')-1) == 9) {echo "Setembro";}?>
                     <?php if ((date('m')-1) == 10) {echo "Outubro";}?>
                     <?php if ((date('m')-1) == 11) {echo "Novenbro";}?>
                     <?php if ((date('m')-1) == 12) {echo "Dezembro";}?> de
                     <?=date('Y')?> 
                 </p>
                </div>
                <div class="content">
                        <?= GoogleChart::widget(array('visualization' => 'ColumnChart',
                            'data' => array(
                                array('Dia', 'Numero Login'),
                                array('1', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 1, 'mes' => (date('m')-1)])->count()),
                                array('2', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 2, 'mes' => (date('m')-1)])->count()),
                                array('3', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 3, 'mes' => (date('m')-1)])->count()),
                                array('4', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 4, 'mes' => (date('m')-1)])->count()),
                                array('5', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 5, 'mes' => (date('m')-1)])->count()),
                                array('6', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 6, 'mes' => (date('m')-1)])->count()),
                                array('7', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 7, 'mes' => (date('m')-1)])->count()),
                                array('8', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 8, 'mes' => (date('m')-1)])->count()),
                                array('9', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 9, 'mes' => (date('m')-1)])->count()),
                                array('10', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 10, 'mes' => (date('m')-1)])->count()),
                                array('11', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 11, 'mes' => (date('m')-1)])->count()),
                                array('12', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 12, 'mes' => (date('m')-1)])->count()),
                                array('13', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 13, 'mes' => (date('m')-1)])->count()),
                                array('14', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 14, 'mes' => (date('m')-1)])->count()),
                                array('15', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 15, 'mes' => (date('m')-1)])->count()),
                                array('16', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 16, 'mes' => (date('m')-1)])->count()),
                                array('17', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 17, 'mes' => (date('m')-1)])->count()),
                                array('18', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 18, 'mes' => (date('m')-1)])->count()),
                                array('20', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 19, 'mes' => (date('m')-1)])->count()),
                                array('21', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 20, 'mes' => (date('m')-1)])->count()),
                                array('22', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 21, 'mes' => (date('m')-1)])->count()),
                                array('22', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 22, 'mes' => (date('m')-1)])->count()),
                                array('23', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 23, 'mes' => (date('m')-1)])->count()),
                                array('24', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 24, 'mes' => (date('m')-1)])->count()),
                                array('25', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 25, 'mes' => (date('m')-1)])->count()),
                                array('26', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 26, 'mes' => (date('m')-1)])->count()),
                                array('27', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 27, 'mes' => (date('m')-1)])->count()),
                                array('28', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 28, 'mes' => (date('m')-1)])->count()),
                                array('29', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 29, 'mes' => (date('m')-1)])->count()),
                                array('30', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 30, 'mes' => (date('m')-1)])->count()),
                                array('31', (int)$infoapp::find()->where(['ano' => date('Y'), 'dia' => 31,'mes' => (date('m')-1)])->count()),
                            ),)); 
                            ?>
                        <div class="footer">
                            <hr>
                            <div class="stats">
                                <i class="ti-reload"></i> Ultima Atualização
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  POI GRAFICOS A PARTIR DE LI DENTO ROW  -->


    </div>
