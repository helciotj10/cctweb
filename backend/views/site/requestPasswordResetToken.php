<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login__form">

    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

    <div class="login__row">
        <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
            <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
        </svg>
        <input id="passwordresetrequestform-email" name="PasswordResetRequestForm[email]" type="text" class="login__input name" placeholder="Introduza seu email"/>      

        <button type="submit" class="login__submit">Enviar Email de Recuparação</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>


