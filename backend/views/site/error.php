<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Este erro ocoreu enquanto o servidor estava processando seu pedido.
    </p>
    <p>
        Por favor contacte-nos se achares que eh um erro do servidor. Obrigado.
    </p>

</div>
