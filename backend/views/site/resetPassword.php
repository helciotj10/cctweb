<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login__form">
    
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <div class="login__row">
                <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                    <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                </svg>
                <input id="resetpasswordform-password" name="ResetPasswordForm[password]" type="text" class="login__input name" placeholder="Introduza nova palavra passe"/>      

                <button type="submit" class="login__submit">Mudar palavra passe</button>
            </div>

            <?php ActiveForm::end(); ?>

</div>
