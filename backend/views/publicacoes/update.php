<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Publicacoes */

$this->title = 'Update Publicacoes: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Publicacoes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="publicacoes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
