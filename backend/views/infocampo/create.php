<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Infocampo */

$this->title = 'Create Infocampo';
$this->params['breadcrumbs'][] = ['label' => 'Infocampos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infocampo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
