<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Infocampo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="infocampo-form">
    
   <?php $form = ActiveForm::begin(); ?>
   <form>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'horaInicio')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'horaFim')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
             <?= $form->field($model, 'horaInicio_FS')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
         </div>
     </div>
     <div class="col-md-6">
        <div class="form-group">
            <?= $form->field($model, 'horaFim_FS')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
           <?= $form->field($model, 'bilhete_nacional')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
       </div>
   </div>

   <div class="col-md-6">
    <div class="form-group">
        <?= $form->field($model, 'bilhete_internacional')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
           <?= $form->field($model, 'telefone')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
       </div>
   </div>

   <div class="col-md-6">
    <div class="form-group">
        <?= $form->field($model, 'endereco')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
    </div>
</div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <?= $form->field($model, 'extras')->widget(\yii\redactor\widgets\Redactor::className(),
                [
                 'clientOptions' => [
                     'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
                 ],
                 ]) ?>
                 
             </div>
         </div>
     </div>
     <div class="text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Salvar Atualizações', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<div class="clearfix"></div>
</form>
<?php ActiveForm::end(); ?>

</div>
