<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InfocampoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Infocampos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infocampo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Infocampo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idInfo',
            'horaInicio',
            'horaFim',
            'telefone',
            'bilhete_nacional',
            // 'bilhete_internacional',
            // 'horaInicio_FS',
            // 'horaFim_FS',
            // 'endereco',
            // 'extras:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
