<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Infocampo */

$this->title = 'Atualizar Informações';
$this->params['breadcrumbs'][] = ['label' => 'Infocampos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idInfo, 'url' => ['view', 'id' => $model->idInfo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="infocampo-update">

	<div class="content">
		<div class="container-fluid">
			<div class="row"> 
				<div class="col-lg-12 col-md-7">
					<div class="card">

						<div class="header">
							<h2 class="title"><?= Html::encode($this->title) ?></h2>
							<br>
							<ol class="breadcrumb" >
								<li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
								<li><a href="index.php?r=infocampo%2Fview&id=1"><i class=""></i> Informações</a></li>
								<li class="active">Atualizar Informações</li>
							</ol>
						</div>
						<div class="content">

							<?= $this->render('_form', [
								'model' => $model,
								]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
