<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InfocampoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="infocampo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idInfo') ?>

    <?= $form->field($model, 'horaInicio') ?>

    <?= $form->field($model, 'horaFim') ?>

    <?= $form->field($model, 'telefone') ?>

    <?= $form->field($model, 'bilhete_nacional') ?>

    <?php // echo $form->field($model, 'bilhete_internacional') ?>

    <?php // echo $form->field($model, 'horaInicio_FS') ?>

    <?php // echo $form->field($model, 'horaFim_FS') ?>

    <?php // echo $form->field($model, 'endereco') ?>

    <?php // echo $form->field($model, 'extras') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
