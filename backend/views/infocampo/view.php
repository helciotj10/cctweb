<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Infocampo */

$this->title = 'Informações Uteis';
//$this->params['breadcrumbs'][] = ['label' => 'Infocampos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infocampo-view">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h2 class="title"><?= Html::encode($this->title) ?></h2>
                                <br>
                                <ol class="breadcrumb" >
                                    <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                                    <li class="active"> Informações</li>
                                </ol>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Hora de Abertura (para visita)</label>
                                                <p class="form-control border-input"> <?=$model->horaInicio?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Hora de Fecho (para visita)</label>
                                                <p class="form-control border-input"> <?=$model->horaFim?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Hora de Abertura Finais de Semana (para visita)</label>
                                                <p class="form-control border-input"> <?=$model->horaInicio_FS?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Hora de Fecho Finais de Semana (para visita)</label>
                                                 <p class="form-control border-input"> <?=$model->horaFim_FS?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Bilhete Nacional</label>
                                                <p class="form-control border-input"> <?=$model->bilhete_nacional?> </p>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Bilhete Internacional</label>
                                                <p class="form-control border-input"> <?=$model->bilhete_internacional?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Telefone</label>
                                                <p class="form-control border-input"> <?=$model->telefone?> </p>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Endereço Eletronico</label>
                                                <p class="form-control border-input"> <?=$model->endereco?> </p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                             <label>Informações Extras</label>
                                            <div class="form-group">
                                                <div class="delemita">
                                                   <?=HtmlPurifier::process($model->extras)?> 
                                                </div>
                                                
                                            </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <a href="index.php?r=infocampo%2Fupdate&id=1" class="btn btn-info btn-fill btn-wd">Atualizar Informações </a>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
