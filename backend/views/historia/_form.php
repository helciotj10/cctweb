<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Historia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historia-form">

    <?php $form = ActiveForm::begin(); ?>

    <form>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= $form->field($model, 'tituloPT')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= $form->field($model, 'historiaPT')->widget(\yii\redactor\widgets\Redactor::className(),
                        [
                         'clientOptions' => [
                             'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
                         ],
                         ]) ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Salvar Atualizações', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <div class="clearfix"></div>
    </form>

    <?php ActiveForm::end(); ?>

</div>
