<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HistoriaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idHistoria') ?>

    <?= $form->field($model, 'tituloPT') ?>

    <?= $form->field($model, 'tituloEN') ?>

    <?= $form->field($model, 'tituloFR') ?>

    <?= $form->field($model, 'tituloES') ?>

    <?php // echo $form->field($model, 'historiaPT') ?>

    <?php // echo $form->field($model, 'historiaEN') ?>

    <?php // echo $form->field($model, 'historiaFR') ?>

    <?php // echo $form->field($model, 'historiaES') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
