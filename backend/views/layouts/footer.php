<footer class="footer">
    <div class="container-fluid">
        <nav class="copyrigh pull-left">
            <ul>
                <li>
                   <a href="http://www.cvmovel.cv/" target="_blank">CVMovel</a>
                </li>
                <li>
                    <a href="http://http://www.unipiaget.cv">UniPiaget</a>
                </li>
                <li>
                    <a href="http://www.unipiaget.cv/index.php?pshow=mnu&p=4&s=32">LED</a>
                </li>
                <li>
                    <a href="https://pt-br.facebook.com/patrimoniocultural.caboverde/">IPC</a>
                </li>
            </ul>
        </nav>
        <div class="copyright pull-right">
           CCT &copy; <script>document.write(new Date().getFullYear())</script>, desenvoldido por <a href="https://www.facebook.com/helcio.ferreira.182">Hélcio Ferreira</a>
        </div>
    </div>
</footer>