<?php
//commit
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

?>

<div class="sidebar" data-background-color="white" data-active-color="danger">

            <!--
                poi class="active" na li
                Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
                Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
            -->

            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="https://www.cctvirtual.cf" class="simple-text">
                        Backend CCT
                    </a>
                </div>

                <ul class="nav">
                    <li>
                        <a href="index.php">
                            <i class="ti-panel"></i>
                            <p>Pagina Inicial</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=historia%2Fview&id=1">
                            <i class="ti-clipboard"></i>
                            <p>Historia</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=visita">
                            <i class="ti-map"></i>
                            <p>Visita Guiada</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=testemunho">
                            <i class="ti-video-camera"></i>
                            <p>Depoimentos</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=midia">
                            <i class="ti-image"></i>
                            <p>Galeria</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=user%2Fview&id=<?=Yii::$app->user->identity->id?>">
                            <i class="ti-user"></i>
                            <p>Perfil do Utlizador </p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=user%2Fusers">
                            <i class="ti-comments-smiley"></i>
                            <p>Utilizadores</p>
                        </a>
                    </li> 
                    <li>
                        <a href="index.php?r=publicacoes">
                            <i class="ti-world"></i>
                            <p>Publicações</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=doacao">
                            <i class="ti-money"></i>
                            <p>Doações</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=infocampo%2Fview&id=1">
                            <i class="ti-info"></i>
                            <p>Informações Uteis</p>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?r=pontosinterece">
                            <i class="ti-direction"></i>
                            <p>Pontos de Interesse</p>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.cctvirtual.cf/downloads">
                            <i class="ti-import"></i>
                            <p>Download</p>
                        </a>
                    </li>

                    <li>
                            
                        <?= Html::a(
                                ' <i class="fa fa-sign-out" aria-hidden="true"></i> <p> <span>Sair da Conta</span> </p> ',
                                ['/site/logout'],
                                ['data-method' => 'post']
                            ) ?>
                    </li>
                </ul>
            </div>
        </div>