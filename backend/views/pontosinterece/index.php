<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PontosintereceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pontos de Interesses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pontosinterece-index">
 
    <div class="panel panel-default" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); padding: 10px;">
        <h2><?= Html::encode($this->title) ?></h2>
        <ol class="breadcrumb" >
            <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"> Pontos de Interece</li>
        </ol>

        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover dataTable js-exportable" style="">
                <thead>
                    <tr>
                        <th><b>Nome</b></th>
                        <th><b>Latitude</b></th>
                        <th><b>Longitude</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        foreach ($dataProvider->getModels() as $model) {

                            echo "<tr class='clickable-row' title='Ver detalhes' data-href='?r=pontosinterece/view&id=".$model->id."' style='cursor: pointer;'>
                                    <td>".$model->nome."</td>
                                    <td>".$model->latude."</td>
                                    <td>".$model->longitude."</td>
                                </tr>";
                        }

                    ?>
                </tbody>
            </table>
        </div>



           
        <br><p>
            
            <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Adicionar Ponto de Interece', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>
</div>
