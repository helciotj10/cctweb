<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pontosinterece */

$this->title = 'Atualizar Pontos de Interesse';
$this->params['breadcrumbs'][] = ['label' => 'Pontosintereces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pontosinterece-update">

	<div class="content">
		<div class="container-fluid">
			<div class="row"> 
				<div class="col-lg-12 col-md-7">
					<div class="card">

						<div class="header">
							<h2 class="title"><?= Html::encode($this->title) ?></h2>
							<br>
							<ol class="breadcrumb" >
								<li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
								<li><a href="index.php?r=pontosinterece"><i class=""></i> Pontos de Interece</a></li>
								<li class="active"> Atualizar Ponto de Interece</li>
							</ol>
						</div>

						<?= $this->render('_form', [
							'model' => $model,
							]) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
