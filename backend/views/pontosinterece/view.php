<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\DetailView;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\LatLng;

$coord = new LatLng(['lat' => $model->latude, 'lng' => $model->longitude]);
    $map = new Map([
        'mapTypeId'=> 'satellite',
        'center' => $coord,
        'zoom' => 19,
            ]);

    $marker = new Marker([
        'position' => $coord,
        'title' => $model->nome,
            ]);

    $marker->attachInfoWindow(
            new \dosamigos\google\maps\overlays\InfoWindow([
        'content' => $model->nome
            ])
    );
    $map->addOverlay($marker);

/* @var $this yii\web\View */
/* @var $model common\models\Pontosinterece */

$this->title = 'Detalhes do Ponto de Interece';
$this->params['breadcrumbs'][] = ['label' => 'Pontosintereces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pontosinterece-view">

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-7">
                    <div class="card">
                        <div class="header">
                            <h2 class="title"><?= Html::encode($this->title) ?></h2>
                            <br>
                            <ol class="breadcrumb" >
                                <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                                <li><a href="index.php?r=pontosinterece"><i class=""></i> Pontos de Interesse</a></li>
                                <li class="active"> Detalhes do ponto de interece</li>
                            </ol>
                        </div>
                        <div class="content">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nome do Ponto de Interesse</label>
                                            <p class="form-control border-input"> <?=$model->nome?> </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Descrição</label>
                                            <div class="form-group">
                                                <div class="delemita">
                                                   <?=HtmlPurifier::process($model->descricao)?> 
                                                </div>
                                                
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Latitude</label>
                                            <p class="form-control border-input"><?=$model->latude?>  </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Longitude</label>
                                            <p class="form-control border-input"><?=$model->longitude?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?=$map->display();?>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <?= Html::a('Atualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                    <?= Html::a('Apagar', ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-danger',
                                        'data' => [
                                            'confirm' => 'Têns a certeza que queres apagar este Ponto de interece?',
                                            'method' => 'post',
                                            'name' => Yii::$app->request->csrfParam,
                                            'value' => Yii::$app->request->csrfToken,
                                            ],
                                        ]) ?>
                                        <?= Html::a('Inserir Midia', ['/midia/create', 'testemunho' => '0', 'historia' => '0', 'pontos' => $model->id], ['class' => 'btn btn-success']) ?>
                                    </div>
                               
                                    
                                    <div class="clearfix"></div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
