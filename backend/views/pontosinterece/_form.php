<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pontosinterece */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pontosinterece-form">
	<div class="card">
		<div class="content">
			<?php $form = ActiveForm::begin(); ?>

			<?= $form->field($model, 'nome')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>

			<?= $form->field($model, 'descricao')->widget(\yii\redactor\widgets\Redactor::className(),
                                                    [
                                                       'clientOptions' => [
                                                           'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
                                                       ],
                                                    ]) ?>

			<?php // $porLog = $form->field($model, 'longitude') ?>
    		<?php // $porLat = $form->field($model, 'latude') ?>

    
    		
    		<?php
	    		echo $form->field($model, 'localisacao')->widget(
	    			'kolyunya\yii2\widgets\MapInputWidget', [
	    				//'porLog' => $porLog,
	    				//'porLat' => $porLat, 
	        			// Google maps browser key. 
	    				'key' => "AIzaSyBYKgzHVAA9C6DiMylA39BDX9jPaLquET4",
	    				'latitude' => 15.263143811041267,
	    				'longitude' => -23.743300437927246,
	    				'zoom' => 16, 

	        			// Coordinates representation pattern. Will be use to construct a value of an actual input.
	        			// Will also be used to parse an input value to show the initial input value on the map.
	        			// You can use two macro-variables: '%latitude%' and '%longitude%'.
	        			// Defaults to '(%latitude%,%longitude%)'.
	    				'pattern' => '%longitude%,%latitude%',
	    				'mapType' => 'satellite',
	    				'alignMapCenter' => false,
	    			]
	    		);
    		?> 
			<div class="text-center">
				<?= Html::submitButton('Guardar Ponto', ['class' => 'btn btn-success']) ?>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>

</div>
