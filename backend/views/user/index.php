<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TestemunhoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Utlizadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testemunho-index">

    <div class="panel panel-default" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); padding: 10px;">
        <h2><?= Html::encode($this->title) ?></h2>
        <ol class="breadcrumb" >
            <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"> Utilizadores</li>
        </ol>

        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover dataTable js-exportable" style="">
                <thead>
                    <tr>
                        <th><b>Foto</b></th>
                        <th><b>Nome</b></th>
                        <th><b>Email</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($dataProvider as $model) {

                        $foto = $model->foto_perfil ? "<img style='width: 50px; height: 50px;' class='avatar border-6white' src='https://www.cctvirtual.cf/downloads/".$model->foto_perfil."' alt='...'/>" : "<img style='width: 50px; height: 50px;' class='avatar border-white' src='uploads/transferir.png' alt='...'/>";
                        echo "<tr>
                        <td>".$foto."</td>
                        <td>".$model->first_name." ".$model->last_name."</td>
                        <td>".$model->email."</td>
                        <td> <div class='icon-big icon-danger text-center'>"; 

                        echo Html::a('<i class="ti-trash"></i>', ['delete', 'id' => $model->id], ['data' => [
                                    'confirm' => 'Tem certeza que pretende Desactivar este utilizador?',
                                    'method' => 'post',
                                    'title' => 'Desactivar Utilizadpr'
                                ],
                            ]);

                        echo " </div> </td>
                        </tr>";
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
