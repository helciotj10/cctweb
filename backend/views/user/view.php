<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="card card-user">
                            <div class="image">
                                <img src="<?=$model->foto_capa?>" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                  <img class="avatar border-white" src="<?=$model->foto_perfil?>" alt="..."/>
                                  <h4 class="title"><?=$model->first_name?> <?=$model->last_name?><br/>
                                     <a href="http://<?=$model->email?>" target="_blank"><small><?=$model->email?></small></a>
                                  </h4>
                                </div>
                                <p class="description text-center">
                                    "<?=$model->descricao?>"
                                </p>
                            </div>
                            <hr>
                            <!--div class="text-center">
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-1">
                                        <h5>12<br /><small>Files</small></h5>
                                    </div>
                                    <div class="col-md-4">
                                        <h5>2GB<br /><small>Used</small></h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>24,6$<br /><small>Spent</small></h5>
                                    </div>
                                </div>
                            </div-->

                        </div>
                        <div class="text-center">
                            <a href="index.php?r=user%2Fupdate&id=<?=Yii::$app->user->identity->id?>" class="btn btn-info btn-fill btn-wd">Atualizar Perfil </a>
                        </div>
                        <br>
                    </div>

                    <div class="col-lg-8 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Perfil do Utilizador</h4>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <input type="text" class="form-control border-input" disabled placeholder="Company" value="IPC">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nome de Utilisador</label>
                                                <p class="form-control border-input"> <?=$model->username?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <p class="form-control border-input"> <?=$model->email?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Primeiro Nome</label>
                                                <p class="form-control border-input"> <?=$model->first_name?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Ultimo Nome</label>
                                                 <p class="form-control border-input"> <?=$model->last_name?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Cidade</label>
                                                <p class="form-control border-input"> <?=$model->cidade?> </p>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Pais</label>
                                                <p class="form-control border-input"> <?=$model->pais?> </p>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Ilha</label>
                                                <p class="form-control border-input"> <?=$model->ilha?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                             <label>Sobre mim</label>
                                            <div class="form-group">
                                                <div class="delemita">
                                                   <?=HtmlPurifier::process($model->about_me)?> 
                                                </div>
                                                
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>

