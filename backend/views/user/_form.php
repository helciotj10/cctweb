<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
   <div class="content">
            <div class="container-fluid">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Atualizar Seu Perfil</h4>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            
                                            <?= $form->field($model, 'fotoc')->widget(FileInput::classname(), [
                                                'name' => 'attachment_48[]',
                                                'options'=>[
                                                    'multiple'=>true
                                                ],
                                                'pluginOptions' => [
                                                    'uploadUrl' => Url::to(['/uploads']),
                                                    'uploadExtraData' => [
                                                        'album_id' => 20,
                                                        'cat_id' => 'Nature'
                                                    ],
                                                    'maxFileCount' => 10
                                                ],
                                            ]) ?>

                                        </div>
                                       
                                        <div class="col-md-6">
                                            <?= $form->field($model, 'fotop')->widget(FileInput::classname(), [
                                                'name' => 'attachment_48[]',
                                                'options'=>[
                                                    'multiple'=>true
                                                ],
                                                'pluginOptions' => [
                                                    'uploadUrl' => Url::to(['/uploads']),
                                                    'uploadExtraData' => [
                                                        'album_id' => 20,
                                                        'cat_id' => 'Nature'
                                                    ],
                                                    'maxFileCount' => 10
                                                ],
                                            ]) ?>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= $form->field($model, 'username')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                 <?= $form->field($model, 'first_name')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= $form->field($model, 'cidade')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= $form->field($model, 'pais')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= $form->field($model, 'ilha')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'descricao')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?= $form->field($model, 'about_me')->widget(\yii\redactor\widgets\Redactor::className(),
                                                    [
                                                       'clientOptions' => [
                                                           'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
                                                       ],
                                                    ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Salvar Atualizações', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div> 

            </div>

    <?php ActiveForm::end(); ?>
</div>
