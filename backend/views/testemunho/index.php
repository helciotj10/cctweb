<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TestemunhoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de testemunhos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testemunho-index">

    <div class="panel panel-default" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); padding: 10px;">
        <h2><?= Html::encode($this->title) ?></h2>
        <ol class="breadcrumb" >
            <li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active"> Testemunhos</li>
        </ol>

        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover dataTable js-exportable" style="">
                <thead>
                    <tr>
                        <th><b>Foto</b></th>
                        <th><b>Nome</b></th>
                        <th><b>Nacionalidade</b></th>
                        <th><b>Preso de</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        foreach ($dataProvider as $model) {

                                $foto = $model->fotoTestemunho ? "<img style='width: 50px; height: 50px;' class='avatar border-6white' src='uploads".$model->fotoTestemunho->foto."' alt='...'/>" : "<img style='width: 50px; height: 50px;' class='avatar border-white' src='uploads/transferir.png' alt='...'/>";
                            echo "<tr class='clickable-row' title='Ver detalhes' data-href='?r=testemunho/view&id=".$model->id."' style='cursor: pointer;'>
                                    <td>".$foto."</td>
                                    <td>".$model->nome."</td>
                                    <td>".$model->nacionalidade."</td>
                                    <td>".$model->data_nascimento."</td>
                                </tr>";
                        }

                    ?>
                </tbody>
            </table>
        </div>



           
        <br><p>
            
            <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Adicionar testemunho', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

   
</div>
