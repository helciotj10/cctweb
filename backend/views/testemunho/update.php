<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Testemunho */

$this->title = 'Atualizar Testemunho';
//$this->params['breadcrumbs'][] = ['label' => 'Testemunhos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="testemunho-update">

	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="header">
							<h2 class="title"><?= Html::encode($this->title) ?></h2>
							<br>
							<ol class="breadcrumb" >
								<li><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
								<li><a href="index.php?r=Testemunho"><i class=""></i> Testemunhos</a></li>
								<li class="active"> Atualizar Testemunhos</li>
							</ol>
						</div>
						<div class="content">

							<?= $this->render('_form', [
								'model' => $model,
								]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
