<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Testemunho */

//$this->title = 'Create Testemunho';
//$this->params['breadcrumbs'][] = ['label' => 'Testemunhos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testemunho-create">

	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="header">
							<h4 class="title">Atualizar Testemunho</h4>
						</div>
						<div class="content">

							<?= $this->render('_form', [
								'model' => $model,
								]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
