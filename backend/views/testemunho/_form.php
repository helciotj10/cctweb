<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Testemunho */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="testemunho-form">

   
   <?php $form = ActiveForm::begin(); ?>
   <form>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
               <?= $form->field($model, 'nome')->textInput(['maxlength' => true,'class' => 'form-control border-input']) ?>
           </div>
       </div>
       <div class="col-md-4">
        <div class="form-group">
           <?= $form->field($model, 'nacionalidade')->dropDownList(['Portuguesa' => 'Portuguesa', 'Angolana' => 'Angolana', 'Cabo-verdiana' => 'Cabo-verdiana', 'Guinieense' => 'Guinieense'], ['prompt'=>'']) ?>
       </div>
   </div>
   <div class="col-md-4">
    <div class="form-group">
      <?= $form->field($model, 'data_nascimento')->widget(
        DatePicker::className(), [
        // inline too, not bad
          'inline' => false, 
         // modify template for custom rendering
         'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
         'clientOptions' => [
          'autoclose' => true,
          'format' => 'dd-M-yyyy'
        ]
        ]); ?>
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
         <?= $form->field($model, 'biografiaPT')->widget(\yii\redactor\widgets\Redactor::className(),
            [
             'clientOptions' => [
                 'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
             ],
             ]) ?>
         </div>
     </div>
 </div>

 <div class="row">
    <div class="col-md-12">
        <div class="form-group">
           <?= $form->field($model, 'estado')->dropDownList(['1' => 'Ativo', '0' => 'Inativo']) ?>
       </div>
   </div>
</div>
<div class="text-center">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar Testemunho' : 'Salvar Atualizações', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
</div>

<div class="clearfix"></div>
</form>
<?php ActiveForm::end(); ?>

</div>
