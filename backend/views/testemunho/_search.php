<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TestemunhoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="testemunho-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nome') ?>

    <?= $form->field($model, 'nacionalidade') ?>

    <?= $form->field($model, 'data_nascimento') ?>

    <?= $form->field($model, 'biografiaPT') ?>

    <?php // echo $form->field($model, 'biografiaEN') ?>

    <?php // echo $form->field($model, 'biografiaFR') ?>

    <?php // echo $form->field($model, 'biografiaES') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
