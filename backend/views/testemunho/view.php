<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model common\models\Testemunho */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Testemunhos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testemunho-view">

     <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div>  
                            <?php
                                $items = array();
                                foreach ($dataProvider->getModels() as $m) {
                                    if ($m->foto === NULL) {
                                        array_push($items, '<iframe src="https://www.youtube.com/embed/'.$m->video.'" frameborder="0"></iframe>');
                                    }
                                    if ($m->foto != NULL) {
                                        
                                        array_push($items, '<img style="width: 100%; height: 500px;" src="uploads'.$m->foto.'" alt="" title="Your image title">');

                                    }
                                }

                                echo yii2mod\bxslider\BxSlider::widget([
                                    'pluginOptions' => [
                                        'maxSlides' => 1,
                                        'minSlides' => 1,
                                        'controls' => true,
                                        'infiniteLoop' => true,
                                        'auto' => true,
                                        'randomStart' => true,
                                        'shrinkItems' => true,

                                         // set video property to true, if in $items array exist videos
                                        'video' => true,
                                        'slideWidth' => 2000,
                                         // usage events
                                        /*'onSliderLoad' => new yii\web\JsExpression('
                                            function() {
                                                console.log("Slider load");
                                            },
                                        ')*/
                                     ],
                                    'items' => $items 
                                 ]);?>
                            </div>
                            <div class="header">
                                <h4 class="title">Detalhes do Testemunho</h4>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                 <label>Nome</label>
                                                <p class="form-control border-input"> <?=$model->nome?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nacionalidade</label>
                                                <p class="form-control border-input"> <?=$model->nacionalidade?> </p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Preso de</label>
                                                <p class="form-control border-input"> <?=$model->data_nascimento?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                               <label>Biografia</label>
                                                <div class="delemita">
                                                   <?=HtmlPurifier::process($model->biografiaPT)?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="form-group">
                                                <label>Estado</label>
                                                <p class="form-control border-input">  <?= $model->fotoTestemunho ? "Ativo" : "Inativo";?> </p>
                                            </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                        <div class="text-center">
                                           <?= Html::a('Atualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                            <?= Html::a('Apagar', ['delete', 'id' => $model->id], [
                                                'class' => 'btn btn-danger',
                                                'data' => [
                                                'confirm' => 'Are you sure you want to delete this item?',
                                                'method' => 'post',
                                                ],
                                            ]) ?>
                                            <?= Html::a('Inserir Midia', ['/midia/create', 'testemunho' => $model->id, 'historia' => '0', 'pontos' => '0'], ['class' => 'btn btn-success']) ?>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
