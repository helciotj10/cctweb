<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\Utilizadores;

/**
 * Password reset request form
 */
class PassresetForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\Utilizadores',
                //'filter' => ['status' => Utilizadores::STATUS_ACTIVE],
                'message' => 'There is no Utilizadores with this email address.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $Utilizadores Utilizadores */
        $Utilizadores = Utilizadores::findOne([
            //'status' => Utilizadores::STATUS_ACTIVE,
            'u_id' => $this->email,
        ]);

        if (!$Utilizadores) {
            return false;
        }
        
        //if (!Utilizadores::isPasswordResetTokenValid($Utilizadores->password_reset_token)) {
            $Utilizadores->generatePasswordResetToken();
            if (!$Utilizadores->save()) {
                return false;
            }
        //}

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordReset-html', 'text' => 'passwordReset-text'],
                ['Utilizadores' => $Utilizadores]
            )
            ->setFrom(['sistemaeconomatos@gmail.com' => 'robot'])
            ->setTo($this->email)
            ->setSubject('Recuperacao de passs ' . Yii::$app->name)
            ->send();
    }
}
