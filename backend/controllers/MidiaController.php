<?php

namespace backend\controllers;

use Yii;
use common\models\Midia;
use common\models\MidiaSearch;
use common\models\Testemunho;
use common\models\TestemunhoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * MidiaController implements the CRUD actions for Midia model.
 */
class MidiaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Midia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MidiaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Midia model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Midia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($testemunho,$historia,$pontos)
    {
        $model = new Midia();

        if ($model->load(Yii::$app->request->post())) {

            $model->idTestemunho = $testemunho;
            $model->idHistoria = $historia;


            $model->fotoc = UploadedFile::getInstance($model,'fotoc');

            if($model->fotoc != NULL){
                $model->fotoc->saveAs('uploads/'.$model->fotoc->baseName.'.'.$model->fotoc->extension);
                $model->foto = '/'.$model->fotoc->baseName.'.'.$model->fotoc->extension;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->idMidia]);
        }

        return $this->render('create', [
            'model' => $model,
            'testemunho' => $testemunho,
            'historia' => $historia,
            'pontos' => $pontos,
        ]);
    }

    /**
     * Updates an existing Midia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->fotoc = UploadedFile::getInstance($model,'fotoc');

            if($model->fotoc != NULL){
                $model->fotoc->saveAs('uploads/'.$model->fotoc->baseName.'.'.$model->fotoc->extension);
                $model->foto = '/'.$model->fotoc->baseName.'.'.$model->fotoc->extension;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->idMidia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Midia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Midia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Midia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Midia::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
