-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Jun-2019 às 16:50
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_cct`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idPublicacao` int(11) NOT NULL,
  `comentario` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`id`, `idUser`, `idPublicacao`, `comentario`) VALUES
(6, 7, 29, 'Bem visitano...'),
(7, 7, 29, 'Lorem Isupur'),
(8, 7, 29, 'teste numero 3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `doacao`
--

CREATE TABLE `doacao` (
  `idDoacao` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `valor` varchar(45) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `historia`
--

CREATE TABLE `historia` (
  `idHistoria` int(11) NOT NULL,
  `tituloPT` varchar(45) DEFAULT NULL,
  `tituloEN` varchar(45) DEFAULT NULL,
  `tituloFR` varchar(45) DEFAULT NULL,
  `tituloES` varchar(45) DEFAULT NULL,
  `historiaPT` longtext,
  `historiaEN` longtext,
  `historiaFR` longtext,
  `historiaES` longtext,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `historia`
--

INSERT INTO `historia` (`idHistoria`, `tituloPT`, `tituloEN`, `tituloFR`, `tituloES`, `historiaPT`, `historiaEN`, `historiaFR`, `historiaES`, `estado`) VALUES
(1, 'Ex-Campo de Concentração do Tarrafal', 'Conse', 'Cam', 'Campo', '<p>O Campo de Concentração do Tarrafal (CCT), é um complexo prisional erguido por força do decreto nº 26.539 de 23 de abril de 1936, para encarcerar presos políticos. Funcionou em duas fases distintas: a primeira (1936-1956) destinado aos presos antifascistas portugueses, e a segunda (1962-1974) para os reclusos anticolonialistas africanos. </p><p>A construção do CCT surgiu na sequencia da instalação do regime autoritário em Portugal em 1926 − o Salazarismo. A sua natureza totalitária levou à deportação de muitos prisioneiros, que ingressaram em revoltas ou manifestações contra a ditadura, ou que eram simplesmente perseguidos. Mais tarde, com o despoletar das guerras coloniais, era a vez dos angolanos, guineenses e caboverdianos serem encarcerados.</p><p>As condições de existência no Campo eram deploráveis. Os trabalhos forçados andavam de mãos dadas com os castigos. Se o preso se recusava a trabalhar, ironicamente na construção do seu próprio enclausuramento, era punido com uma panóplia de castigos: o isolamento, as humilhações, a estátua, a tortura do sono, o segredo, os espancamentos eram algumas das práticas desumanas, devidamente aprovadas pela cúpula do regime fascista. A “Frigideira” construída em 1937 e substituída na 2ª fase de funcionamento do CCT pela “Holandinha” foram celas, de reduzidas dimensões, edificadas para perpetuar os castigos aplicados aos reclusos.</p><p>As magras rações, as doenças, juntamente com as parcas condições sanitárias contribuíram de forma decisiva pela alta taxa de mortalidade verificada no CCT. A assistência médica era de cariz assassina destacando-se o papel do médico, Esmeraldo Pais da Prata, que sentenciou a vida de muitos presos com a famigerada frase «NÃO ESTOU AQUI PARA CURAR, MAS PARA ASSINAR CERTIDÕES DE OBITOS»!!!</p><p>Pelo CCT passaram mais de 500 prisioneiros, entre antifascistas e anticolonialistas, sendo que ceifou a vida a cerca de 37 reclusos, para além das marcas físicas e psicológicas irreversíveis a que muitos foram sujeitos. Foi desativado a 1 de maio de 1974, na sequência da revolução dos cravos, em Portugal, colocando fim a um lugar que passou para a posteridade como o epiteto de “Campo da morte lenta”. </p>', 'bla bla bla', 'bla bla bla', 'bla bla bla', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `infoapp`
--

CREATE TABLE `infoapp` (
  `id` int(11) NOT NULL,
  `dowload` int(11) DEFAULT '0',
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL,
  `login` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `infoapp`
--

INSERT INTO `infoapp` (`id`, `dowload`, `dia`, `mes`, `ano`, `login`) VALUES
(1, 1, 0, 10, 0, 0),
(9, 0, 1, 10, 2018, 0),
(10, 0, 2, 10, 2018, 0),
(11, 0, 3, 10, 2018, 0),
(12, 0, 4, 10, 2018, 0),
(13, 0, 5, 10, 2018, 0),
(14, 0, 6, 10, 2018, 0),
(15, 0, 7, 10, 2018, 0),
(16, 0, 8, 10, 2018, 0),
(17, 0, 9, 10, 2018, 0),
(18, 0, 10, 10, 2018, 0),
(19, 0, 11, 10, 2018, 0),
(20, 0, 12, 10, 2018, 0),
(21, 0, 13, 10, 2018, 0),
(22, 0, 14, 10, 2018, 0),
(23, 0, 15, 10, 2018, 0),
(24, 0, 16, 10, 2018, 0),
(25, 0, 17, 10, 2018, 0),
(26, 0, 18, 10, 2018, 0),
(27, 0, 19, 10, 2018, 0),
(28, 0, 20, 10, 2018, 0),
(29, 0, 21, 10, 2018, 0),
(30, 0, 22, 10, 2018, 0),
(31, 0, 23, 10, 2018, 0),
(32, 0, 24, 10, 2018, 0),
(33, 0, 25, 10, 2018, 0),
(34, 0, 26, 10, 2018, 0),
(35, 0, 27, 10, 2018, 0),
(36, 0, 28, 10, 2018, 0),
(37, 0, 29, 10, 2018, 0),
(38, 0, 1, 10, 2018, 0),
(39, 0, 5, 10, 2018, 0),
(40, 0, 18, 10, 2018, 0),
(41, 0, 21, 10, 2018, 0),
(42, 0, 18, 10, 2018, 0),
(43, 0, 9, 10, 2018, 0),
(44, 0, 17, 10, 2018, 0),
(45, 0, 16, 10, 2018, 0),
(46, 0, 24, 10, 2018, 0),
(48, 0, 30, 10, 2018, 0),
(49, 1, 22, 11, 2018, 0),
(50, 0, 22, 11, 2018, 1),
(51, 0, 5, 12, 2018, 1),
(52, 0, 5, 12, 2018, 1),
(53, 0, 5, 12, 2018, 1),
(54, 0, 14, 12, 2018, 1),
(55, 0, 14, 12, 2018, 1),
(56, 0, 14, 12, 2018, 1),
(57, 0, 14, 12, 2018, 1),
(58, 0, 14, 12, 2018, 1),
(59, 0, 14, 12, 2018, 1),
(60, 0, 15, 12, 2018, 1),
(61, 0, 15, 12, 2018, 1),
(62, 0, 15, 12, 2018, 1),
(63, 0, 15, 12, 2018, 1),
(64, 0, 15, 12, 2018, 1),
(65, 0, 15, 12, 2018, 1),
(66, 0, 15, 12, 2018, 1),
(67, 0, 15, 12, 2018, 1),
(68, 0, 15, 12, 2018, 1),
(69, 0, 16, 12, 2018, 1),
(70, 0, 16, 12, 2018, 1),
(71, 0, 16, 12, 2018, 1),
(72, 0, 16, 12, 2018, 1),
(73, 0, 16, 12, 2018, 1),
(74, 0, 16, 12, 2018, 1),
(75, 0, 16, 12, 2018, 1),
(76, 0, 16, 12, 2018, 1),
(77, 0, 16, 12, 2018, 1),
(78, 0, 16, 12, 2018, 1),
(79, 0, 16, 12, 2018, 1),
(80, 0, 16, 12, 2018, 1),
(81, 0, 16, 12, 2018, 1),
(82, 0, 16, 12, 2018, 1),
(83, 0, 16, 12, 2018, 1),
(84, 0, 16, 12, 2018, 1),
(85, 0, 16, 12, 2018, 1),
(86, 0, 16, 12, 2018, 1),
(87, 0, 16, 12, 2018, 1),
(88, 0, 16, 12, 2018, 1),
(89, 0, 16, 12, 2018, 1),
(90, 0, 16, 12, 2018, 1),
(91, 0, 16, 12, 2018, 1),
(92, 0, 16, 12, 2018, 1),
(93, 0, 16, 12, 2018, 1),
(94, 0, 16, 12, 2018, 1),
(95, 0, 16, 12, 2018, 1),
(96, 0, 16, 12, 2018, 1),
(97, 0, 16, 12, 2018, 1),
(98, 0, 16, 12, 2018, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `infocampo`
--

CREATE TABLE `infocampo` (
  `idInfo` int(11) NOT NULL,
  `horaInicio` varchar(5) DEFAULT NULL,
  `horaFim` varchar(5) DEFAULT NULL,
  `telefone` varchar(10) DEFAULT NULL,
  `bilhete_nacional` varchar(10) DEFAULT NULL,
  `bilhete_internacional` varchar(10) DEFAULT NULL,
  `horaInicio_FS` varchar(45) DEFAULT NULL,
  `horaFim_FS` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `extras` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `infocampo`
--

INSERT INTO `infocampo` (`idInfo`, `horaInicio`, `horaFim`, `telefone`, `bilhete_nacional`, `bilhete_internacional`, `horaInicio_FS`, `horaFim_FS`, `endereco`, `extras`) VALUES
(1, '9:00', '17:00', '2624575', '100 CVE', '2 EUR', '9:00', '17:00', 'nospatrimonio@gmail.com', 'No dia internacional dos museus, 18 de Maio, é gratuita a entrada no Museu da Resistência-Ex Campo de Concentração do Tarrafal');

-- --------------------------------------------------------

--
-- Estrutura da tabela `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `idPublicacao` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `likes`
--

INSERT INTO `likes` (`id`, `idPublicacao`, `idUser`) VALUES
(80, 26, 7),
(81, 28, 7),
(82, 27, 7),
(85, 29, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE `midia` (
  `idMidia` int(11) NOT NULL,
  `video` varchar(500) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `descricaoPT` longtext,
  `descricaoEN` longtext,
  `descricaoFR` longtext,
  `descricaoES` longtext,
  `estado` tinyint(1) NOT NULL,
  `idHistoria` int(11) DEFAULT NULL,
  `idTestemunho` int(11) DEFAULT NULL,
  `idPontos` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`idMidia`, `video`, `foto`, `tipo`, `descricaoPT`, `descricaoEN`, `descricaoFR`, `descricaoES`, `estado`, `idHistoria`, `idTestemunho`, `idPontos`) VALUES
(12, '', '/20140101_155028.jpg', 1, 'Ex-Campo de Concentração do Tarrafal', NULL, NULL, NULL, 1, 1, NULL, ''),
(18, '', '/20140101_175553.jpg', 1, 'Cela dos presos políticos caboverdianos', NULL, NULL, NULL, 1, 1, 2, ''),
(25, '', '/20140101_150501.jpg', 1, '', NULL, NULL, NULL, 1, 1, NULL, ''),
(26, '', '/20140101_150433.jpg', NULL, 'Cela de presos políticos Guineenses ', NULL, NULL, NULL, 1, NULL, NULL, NULL),
(27, '', '/20140101_175148.jpg', NULL, '', NULL, NULL, NULL, 1, NULL, NULL, NULL),
(28, '', '/20140101_180932.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(29, '', '/20140101_181042.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(30, '', '/20140101_155242.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(31, '', '/20140101_150400.jpg', 2, '', NULL, NULL, NULL, 1, NULL, NULL, NULL),
(32, '', '/20140101_180232.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(33, '', '/20140101_155439.jpg', NULL, '', NULL, NULL, NULL, 1, NULL, NULL, NULL),
(35, '', '/20140101_174546.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(36, '', '/campo concentração tarrafal.png', 1, 'Mapa do campo de concentração do tarrafal', NULL, NULL, NULL, 1, NULL, NULL, ''),
(37, '', '/20140101_175039.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(38, '', '/20140101_175451.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(39, '', '/20140101_175233.jpg', 2, '', NULL, NULL, NULL, 1, 1, NULL, NULL),
(40, '', '/20140101_175626.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(41, '', '/20140101_175834.jpg', NULL, '', NULL, NULL, NULL, 1, 1, NULL, NULL),
(42, '', '/20140101_175738.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(43, '', '/20140101_180835.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(44, '', '/20140101_180614.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(47, '', '/20140101_155136.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(48, '', '/20140101_180810.jpg', NULL, 'Cozinha', NULL, NULL, NULL, 1, 0, 0, NULL),
(49, '', '/20140101_180118.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 0, NULL),
(60, '', '/Jaime Schofield 1970 - 73.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 11, ''),
(62, '', '/Alberto Sanches Semedo 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 13, ''),
(63, '', '/Luís Fonseca 1969 - 73.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 14, ''),
(64, '', '/João Divo Macedo 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 15, ''),
(65, '', '/Fernando dos Reis Tavares -(Toco) 1970.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 16, ''),
(66, '', '/Luís Furtado Mendonça 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 17, ''),
(67, '', '/José Maria Ferreira Querido (Zeki) 1971.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 18, ''),
(68, '', '/António Pedro da Rosa 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 19, ''),
(69, '', '/Gil Querido Varela (Kid) 1971.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 20, ''),
(70, '', '/Arlindo dos Reis Borges 1970-74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 21, ''),
(71, '', '/Juvêncio da Veiga (Dissanto) 1970-74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 22, ''),
(72, '', '/Pedro Martins 1970-74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 23, ''),
(73, '', '/Joaquim Mendes Correia 1970-74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 24, ''),
(74, '', '/Carlos Dantas Tavares 1970 - 73.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 25, ''),
(75, '', '/Ananias Gomes Cabral (Goti) 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 12, ''),
(76, '', '/Manuel Bernardo de Sousa 1962 - 69.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 26, ''),
(77, '', '/José Diogo Ventura 1962 - 69.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 27, ''),
(78, '', '/André Franco de Sousa 1962 - 63.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 28, ''),
(79, '', '/João Fialho da Costa 1962 - 65.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 29, ''),
(80, '', '/Carlos Alberto Van-Dúnem (Beto)1962-67.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 30, ''),
(81, '', '/Jaime Cohen 1970 - 73.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 31, ''),
(82, '', '/Lote Sanguia 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 32, ''),
(83, '', '/Lote Sachicuenda 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 33, ''),
(84, '', '/Vicente Pinto de Andrade 1970-74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 34, ''),
(85, '', '/Augusto Kiala Bengue 1970-74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 35, ''),
(86, '', '/José Luandino Vieira 1064 -1972.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 36, ''),
(87, '', '/Justino Pinto de Andrade 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 37, ''),
(88, '', '/Joel Pessoa 1970 - 74.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 38, ''),
(89, '', '/Armando Ferreira da Conceição Júnior 1962-71.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 39, ''),
(90, '', '/Manuel das Neves Trindade 1962 - 64.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 40, ''),
(91, '', '/Constantino Lopes da Graça 1962 - 67.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 41, ''),
(92, '', '/Carlos Sambú 1962 - 69.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 42, ''),
(93, '', '/Candido Joaquim da Costa 1962 - 69.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 43, ''),
(94, '', '/Candido Joaquim da Costa 1962 - 69.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 44, ''),
(95, '', '/Macário Freire Monteiro 1962 - 68.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 45, ''),
(96, '', '/Caramó Sanhá 1962-64.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 46, ''),
(97, '', '/Mário Soares 1962-1969.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 47, ''),
(98, '', '/Augusto Pereira da Graça 1962-69.jpg', NULL, '', NULL, NULL, NULL, 1, 0, 48, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1511181636),
('m130524_201442_init', 1511181642);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pontosinterece`
--

CREATE TABLE `pontosinterece` (
  `id` int(11) NOT NULL,
  `nome` varchar(500) DEFAULT NULL,
  `descricao` longtext,
  `latude` varchar(500) DEFAULT NULL,
  `longitude` varchar(500) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pontosinterece`
--

INSERT INTO `pontosinterece` (`id`, `nome`, `descricao`, `latude`, `longitude`, `categoria`) VALUES
(1, 'Igreja Matriz Santo Amaro Abade ', '<p>situado no centro da cidade nos arredores da praça do município, ela foi reconstruída em 1947 e inaugurada em 1954. Ela possui uma arquitetura bastante rica, com destaque no seu interior pelos 14 estacões da via-sacra da Paixão de Jesus Cristo, um coro e uma sacristia contígua. Esta igreja representa o poder sagrado no Concelho. </p>', '15.263550639060261', '-23.741701841436225', NULL),
(2, 'Cemitério de Achada Baixo ', '<p>intimamente ligado ao ex-Campo de Concentração, nele podem ver-se campas de 37 antifascistas portugueses, incluindo a do fundador e líder do Partido Comunista Português, Bento Gonçalves, cujos restos mortais foram transladados para Portugal, nos finais da década de 80 – do século passado. </p>', '15.264627075424176', '-23.741455078125', NULL),
(3, 'Mercado Municipal da Cultura ', '<p>circunvizinho à praça municipal, foi construído em 1935 e recentemente remodelado e atribuído a função espaço de cultura do município<o:p></o:p></p>', '15.264569861351958', '-23.741390705108643', NULL),
(4, '	Centro de Artesanato de Trás-os-Montes ', '<p>destaca-se pela preservação e produção da olaria tradicional do concelho do Tarrafal. Ali produz-se utensílios de uso domestico e decorativo.<o:p></o:p></p>', '15.263301079660712', '-23.742833733558655', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes`
--

CREATE TABLE `publicacoes` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `foto` longtext,
  `descricao` varchar(500) NOT NULL,
  `gosto` int(11) NOT NULL,
  `comentario` int(11) NOT NULL DEFAULT '0',
  `dia` varchar(100) DEFAULT NULL,
  `mes` varchar(100) DEFAULT NULL,
  `ano` varchar(100) DEFAULT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `publicacoes`
--

INSERT INTO `publicacoes` (`id`, `idUser`, `foto`, `descricao`, `gosto`, `comentario`, `dia`, `mes`, `ano`, `estado`) VALUES
(29, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(30, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(31, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(32, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(33, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(34, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(35, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(36, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(37, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(38, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(39, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(40, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(41, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(42, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(43, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(44, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(45, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(46, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(47, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(48, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(49, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(50, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(51, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1),
(52, 7, 'uploads/2343eb8984229164a10aee9f7e9d9464.png', 'Nos museu, nos historia.', 1, 6, '14', 'Dezembro', '2018', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `testemunho`
--

CREATE TABLE `testemunho` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `nacionalidade` varchar(100) DEFAULT NULL,
  `data_nascimento` varchar(10) DEFAULT NULL,
  `biografiaPT` longtext,
  `biografiaEN` longtext,
  `biografiaFR` longtext,
  `biografiaES` longtext,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `testemunho`
--

INSERT INTO `testemunho` (`id`, `nome`, `nacionalidade`, `data_nascimento`, `biografiaPT`, `biografiaEN`, `biografiaFR`, `biografiaES`, `estado`) VALUES
(11, 'Jaime Scofield', 'Cabo-verdiana', '1970-73', '<p>O nosso dia-dia era completamente preenchido: cuidávamos da higiene da cela, lavávamos os pratos dos 17 – quatro de Barlavento e treze de Santiago. Dois limpavam a cela, punham água, isso logo de manhã cedo. Organizamos os estudos, quem não tina a quarta classe fez, quem não tina completado o liceu tambem fez. Fazíamos artesanato e ginástica, uma vez por semana lavávamos a nossa roupa… E graças a isso tivemos uma boa qualidade de saúde, quer melhorando a comida, quer com aquilo que a família podia enviar.</p>', NULL, NULL, NULL, 1),
(12, 'Ananias Gomes Cabral, Goti', 'Cabo-verdiana', '1970-74', '<p>(…) fiz tudo para evitar chatices. Castigo bastava-me o que tinha tido na Praia, na Cadeia Civil. A noite, quando dava o silencio, nós reuníamos as escondidas – 17 homens. Não tínhamos outra alternativa.</p>', NULL, NULL, NULL, 1),
(13, 'Alberto Sanches Semedo', 'Cabo-verdiana', '1970-74', '<p>Eu não digo que o governo de Cabo Verde «ensodou», porque muito se fez, mas há coisas que é necessário fazer. O Campo devia ficar como um lugar histórico para os nossos filhos, os nossos netos conhecerem, um museu. Para que um dia se posso dizer: «o teu pai passou por aqui». Num museu, com pessoas com um certo conhecimento do edifício, capaz de explicar que por lá passaram portugueses, guineenses, angolanos, cabo-verdianos…</p>', NULL, NULL, NULL, 1),
(14, 'Luís Fonseca', 'Cabo-verdiana', '1969-73', '<p>A entrada numa prisão como Tarrafal não pode deixar ficar gravada na memória. Fomos acolhidos por um senhor de cabelo branco, de farde caqui, muito hirto, magrinho, que era diretor, o [Eduardo Vieira] fontes. Cabo-verdiano, mas era funcionário colonial de Angola, e ele esforçava-se para mostrar o seu zelo e dedicação na reeducação dos presos. Estava ele, o chefe dos guardas, um português, o sr. Reis. Receberam-nos com um sermão muito comovente, a dizer que nós íamos lá para sermos reeducados, para podermos voltar a abraçar os valores da civilização ocidental cristã e os valores da portugalidade.</p>', NULL, NULL, NULL, 1),
(15, 'João Divo de Macedo', 'Cabo-verdiana', '1970-74', '<p>Eu naquela altura era muito jovem, (…), eu tinha 17 anos. Não senti de imediato; aos poucos, dentro da cadeia, e que fui tomando noção daquilo. Eu entrei normal…</p>', NULL, NULL, NULL, 1),
(16, 'Fernando dos Reis Tavares, “Toco”', 'Cabo-verdiana', '1970', '<p>No pouco tempo em que lá estivemos conseguimos, da parte dos colegas, uma camaradagem sã. Eu tinha, e tenho, dificuldades com o inglês, o Luís foi nosso professor, o Lineu na História, o Carlos na Geografia...Aquilo era uma escola. À noite, depois do jantar, reuníamos e estudávamos. Cada um apresentava a sua lição. A nossa maior força era à espera de que um momento para o outro o PAIGC nos ia tirar de lá. Aquilo dávamos um grande alento. Sabíamos que a vitória estava do nosso lado.</p>', NULL, NULL, NULL, 1),
(17, 'Luís Furtado Mendonça', 'Cabo-verdiana', '1970-74', '<p>Foi uma saturação muito grande e ao mesmo tempo uma grande coragem. Eu tinha deixado mulher, filhos, mãe… Mas, como já estávamos destinados, tudo isso foi esquecido a partir do momento que entrei lá dentro. Tudo foi esquecido porque estávamos a lutar por um fim, que era a independência de Cabo Verde.</p>', NULL, NULL, NULL, 1),
(18, 'José Maria Ferreira Querido, Zeki', 'Cabo-verdiana', '1971', '<p>Era terrível. Nos primeiros meses não tínhamos sol. Com três quartos vazios, o Dadinho Fontes pôs-nos num quarto nós os três [Toco e Kid Varela]. Tínhamos uma lata de água para beber e outra para defecar. Não podíamos sair. Para ele isso era um “bom tratamento”. </p>', NULL, NULL, NULL, 1),
(19, 'António Pedro da Rosa', 'Cabo-verdiana', '1970-74 ', '<p>Uma sensação horrível. Ao chegarmos levaram-nos para o gabinete do chefe do campo e ele começou a ler o regulamento interno. Aquilo deu-nos um “corte” na barriga, de tão horrível. E, para causar ainda mais impressão, ele dava ênfase àquelas instruções, de modo a nos sentirmos aterrorizados por dentro. </p>', NULL, NULL, NULL, 1),
(20, 'Gil Querido Varela, Kid', 'Cabo-verdiana', '1971', '<p>No Tarrafal outra coisa que me marcou foi a holandinha. Nenhum de nós três foi para lá, mas puseram um angolano lá e como tínhamos mobilizado um dos polícias, através dele, na hora da refeição, deixávamos um prato para esse polícia levar a esse preso angolano.</p>', NULL, NULL, NULL, 1),
(21, 'Arlindo Reis Borges', 'Cabo-verdiana', '1970-74', '<p>Nós quando fomos ao Tarrafal já não havia tortura, aquilo era prisão. A tortura era psicológica, digamos assim. Estávamos privados da liberdade, a alimentação não era boa, até adoeci, o médico disse que era uma desinteria por causa de peixe estragado.</p>', NULL, NULL, NULL, 1),
(22, 'Juvêncio da Veiga, Dissanto', 'Cabo-verdiana', '1970-74', '<p>Nós, que não tínhamos estudos, comprámos livros para o Lineu, o Luís Fonseca e o Pedrinho Damas nos ensinarem a pôr o nome, e escrever, a ler. Não tínhamos outro tipo de convivência naquela altura.</p>', NULL, NULL, NULL, 1),
(23, 'Pedro Martins', 'Cabo-verdiana', '1970-74', '<p>(…) uma coisa é passar por lá, mesmo com a vigilância da PIDE, o nervosismo que se sente quando se entra num campo dessa natureza, e outra é entrar como preso, sem dia de sair. A estrutura física do complexo, a presença militar, paramilitar, a organização do campo, eram forças que levavam a diminuir o sentido e a dimensão da pessoa que estava aí dentro. Aquilo não era Cabo Verde e não era um ambiente, com certeza social, com pessoas com as quais se podia dialogar. </p>', NULL, NULL, NULL, 1),
(24, 'Joaquim Correia', 'Cabo-verdiana', '1970-74', '<p>O meu primeiro contacto com o Tarrafal foi em 1957 quando o meu cunhado me acompanhou ao Tarrafal para eu fazer o exame de terceira classe, o primeiro grau. (…) Nessa viagem reparei que ia um preso para Chão Bom; o carro, quando chegou, foi direto ao campo. Foi aí que eu vi o campo pela primeira vez. </p>', NULL, NULL, NULL, 1),
(25, 'Carlos Dantas Tavares', 'Cabo-verdiana', '1970-73', '<p>Já tínhamos tidos dois anos e tal de prisão, na Cadeia Civil da Praia, por isso, o Tarrafal era a continuidade disso. Ainda pensamos na altura que ia ser a continuidade com ligeira melhoria. Foi o que disseram: “ah, o Tarrafal tem melhores condições”. Mas na verdade foi pior. E foi pior principalmente por causa do diretor, o Dadinho Fontes.</p>', NULL, NULL, NULL, 1),
(26, 'Manuel Bernardo de Sousa', 'Angolana', '1962-69', '<p>Fomos julgados (…) e enviados para Tarrafal. Fomos daqui num avião DC 8, partimos às 9 horas da noite, no aeroporto militar directo para a ilha do Sal. Desembarcámos e nos colocaram numa zona que era um antigo curral de cabras. Aí aguardámos o navio que nos conduziu até à ilha de Santiago. Já não me lembro em que horas partimos, mas chegámos a Santiago já à noitinha. No porto do Tarrafal, encontrámos um camião militar que nos levou até ao Campo de Trabalho de Chão Bom. Fomos recebidos e introduzidos numa sala, postos todos nus…</p>', NULL, NULL, NULL, 1),
(27, 'José Diogo Ventura', 'Angolana', '1962-69', '<p>(…), já tinha ouvido falar do Tarrafal, mas devo dizer que não tinha nenhuma noção do que aquilo era. Líamos nos jornais sobre algumas prisões que tinham sido efetuadas em Portugal, presos políticos, que eram depois julgados no Tribunal da Boa Hora, e através disso ouvia-se falar que tinham sido presos, em tempos anteriores, outros portugueses e que tinham estado no Tarrafal. Nem sabíamos se eles ainda la estavam ou se já tinham saído, só quando lá chegamos e que vimos que já la não estavam. </p>', NULL, NULL, NULL, 1),
(28, 'André Franco de Sousa', 'Angolana', '1962-63', '<p>Foi uma surpresa para todos a informação de que íamos para Cabo Verde. O campo do Tarrafal tinha sido aberto para os portugueses, com a campanha internacional, o governo português encerrou-o. Os últimos presos que la estavam foram enviados para a Colónia Penal do Bié, outros para Portugal, e o campo foi encerrado. Só quando se deu o 4 de Fevereiro [de 1961], eles pensaram que aquilo ia-se complicar, é que decidiram mandar-nos para o Tarrafal.</p>', NULL, NULL, NULL, 1),
(29, 'João Fialho da Costa', 'Angolana', '1962-65', '<p>Quando chegámos ao Tarrafal foi impressionante. Era tempo de queda de folha das arvores, acácias, os ramos pareciam almas do outro mundo. Mandaram-nos tirar a roupa e ficamos completamente nus e entramos para acaserna que estava vazia. A entrada vimos camas com colchões, lençóis, colcha. E ironizamos: «afinal ainda se morre com glória. Estão a ver o que é uma coisa bem feita!» Fecharam-nos, no dia seguinte, de manhã, as seis horas, abriram-nos as portas para irmos a refeitório.</p>', NULL, NULL, NULL, 1),
(30, 'Carlos Alberto Van-Dunem', 'Angolana', '1962-67', '<p>Havia muita gente na rua, nas janelas, nas portas, a espreitar ou a olhar. E nós subimos, a pé, com as nossas malas, até ao Chão Bom. Chegamos, entrámos na cadeia, que era dividida. Tinha a entrada, e do lado direito de quem entrasse estava a cadeia dos presos políticos e do lado esquerdo a dos de delito comum.</p>', NULL, NULL, NULL, 1),
(31, 'Jaime Cohen', 'Angolana', '1970-73', '<p>Eu saí do Tarrafal um pouco antes [1974], porque a minha família e outras, tentaram através de advogados progressistas portugueses um “habeas corpus”. O Juca Valentim (saiu primeiro), eu, o Bernardo Teixeira e o Gilberto [Saraiva de Carvalho] fomos retirados do Tarrafal e levados para Lisboa, onde estivemos alguns dias em Caxias, e depois enviados para o Campo de Concentração de S. Nicolau, no Sul de Angola. Afinal de contas, o esperado beneficio do “habeas corpus” resultou uma situação ainda pior. As condições prisionais eram bem piores que no Tarrafal.</p>', NULL, NULL, NULL, 1),
(32, 'Lote Sanguia', 'Angolana', '1970-74', '<p>Depois de algum tempo é que alguns passaram a trabalhar como carpinteiros, serventes, mas só para apanhar ar, no quintal. Também trabalhámos nas estradas, fazendo massa para os pedreiros, fazendo pontes e outras obras. Estivemos nisso durante algum tempo. </p>', NULL, NULL, NULL, 1),
(33, 'Lote Sachicuenda', 'Angolana', '1970-74', '<p>Felizmente, depois de cinco anos, aconteceu o 25 de Abril. Surpreendentemente, no dia 1 de Maio de 1974, ouvimos muito barulho. “Angolanos, liberdade!” Toda a gente lá dentro muito assustada. “Que é isso, o que se passa?”</p>', NULL, NULL, NULL, 1),
(34, 'Vicente Pinto de Andrade', 'Angolana', '1970-74 ', '<p>A ideia principal é: “vim para aqui e não sei se sairei daqui”. Sabíamos que tínhamos ido para um sítio que era duro, onde tinha havido pessoas que morreram, quer portugueses, quer guineenses, e portanto a expectativa é que só sairíamos se o PAIGC conseguisse libertar-nos ou então Cabo Verde e a Guiné-Bissau conseguissem ser independentes. </p>', NULL, NULL, NULL, 1),
(35, 'Augusto, Kiala Bengue', 'Angolana', '1970-74 ', '<p>No caso de Cabo Verde [Tarrafal], tudo dependia, muitas vezes, da forma como a pessoa se comportava. Por causa de indisciplina, houve quem tivesse sido posto no segredo, a pão e água. Mas em comparação com aqui [Angola], para mim, aqui foi mais severo, fui mesmo maltratado. </p>', NULL, NULL, NULL, 1),
(36, 'José Luandino Vieira', 'Angolana', '1964-1972', '<p>Quando chegou o grupo do Justino, Vicente e outros, eles vinham todos empolgados e nós lhes dissemos: “Eh pá, cuidado, que aqui…” Houve logo um que começou a gravar na madeira da mesa “MPLA, vitória ou morte”. Eu disse: “Isto não tem nenhum sentido, a não ser o sentido pessoal. Vocês vejam lá porque nós aqui já temos um mínimo de organização. Aqui é resistir”. Resistir, claro, naquelas condições. </p>', NULL, NULL, NULL, 1),
(37, 'Justino Pinto de Andrade', 'Angolana', '1970-74', '<p>Fomos encostados e um sítio, todos em fila, revistados, depois levados para o campo. O [diretor] Eduardo Fontes foi ter connosco, apresentou-se. Ele era um individuo com uma educação meio marcial – tinha feito o curso de administração colonial e tinha sido administrador cá em Angola. Claro, deu-nos as boas-vindas, e disse quais eram os nossos direitos e quais os nossos deveres. E a isso ele juntou um pouco aquele carácter diplomático, que tinha também. </p>', NULL, NULL, NULL, 1),
(38, 'Joel pessoa', 'Angolana', '1970-74', '<p>(…) o Tarrafal era uma prisão para o resto da vida. Se não fosse o 25 de Abril nós iríamos morrer todos lá. Se não fosse o 25 de Abril nós não éramos nada. </p>', NULL, NULL, NULL, 1),
(39, 'Armando Ferreira da Conceição', 'Angolana', '1962-71', '<p>Logo á entrada o diretor da cadeia, Queimado Pinto, mandou-nos tirar a roupa toda. “Estes terroristas, tirar a roupa toda, todo o mundo!” A desconfiar que trouxéssemos armas. E nisso havia pais e filhos, um dos filhos, levanta-se (primeiro mandou-nos sentar no chão) e dirige-se a ele: “Senhor diretor, desculpe, mas aquele velho ali é meu pai, tenho o pudor e o respeito de não me despir diante dele…” “Não quero saber, nem quero ouvir mais nada, trate de tirar a roupa!”</p>', NULL, NULL, NULL, 1),
(40, 'Manuel Neves Trindade', 'Guinieense', '1962-64', '<p>Assim que chegámos a terra entrámos num carro e fomos levados [Chão Bom]. No caminho os polícias faziam-nos olhar só para baixo, sem levantar a cabeça, ate chegarmos a prisão. Fomos metidos nas celas e lá ficamos, deitamos no chão.</p>', NULL, NULL, NULL, 1),
(41, 'Constantino Lopes da Costa', 'Guinieense', '1962-67', '<p>Primeiro passei alguns meses em Bissau, na Segunda Esquadra, depois levaram-me para a prisão de Mansoa, lá passei também alguns meses, e só depois é que eu e um grupo (éramos cem pessoas) fomos enviados para a ilha das Galinhas e daqui para Tarrafal.</p>', NULL, NULL, NULL, 1),
(42, 'Carlos Sambú', 'Guinieense', '1962-69', '<p>No início não fazíamos nada, mas depois que ganharam confiança em nós passaram a permitir que fizéssemos alguns trabalhos. Eu, por exemplo, pintava. Havia algumas obras dentro do campo e permitiam que trabalhássemos nisso.</p>', NULL, NULL, NULL, 1),
(43, 'Cândido Joaquim da Costa', 'Guinieense', '1962-69', '<p>Na hora de embarcar eu e um colega, Benjamim de Barros, vimos escrito num envelope grande: «Seguem para CV». Logo desconfiamos que seria Cabo Verde, éramos dos poucos que sabíamos ler, mas não dissemos nada a ninguém.</p>', NULL, NULL, NULL, 1),
(44, 'Cândido Joaquim da Costa', 'Guinieense', '1962-69', '<p>Na hora de embarcar eu e um colega, Benjamim de Barros, vimos escrito num envelope grande: «Seguem para CV». Logo desconfiamos que seria Cabo Verde, éramos dos poucos que sabíamos ler, mas não dissemos nada a ninguém.</p>', NULL, NULL, NULL, 1),
(45, 'Macário Freire Monteiro', 'Guinieense', '1962-68', '<p>Havia lá gente boa. Eu lembro-me dos cozinheiros – nho Fidalgo, Ildo… - quando precisamos alguma coisa, deixávamos dinheiro escondido num lugar, ao passarmos por eles, para ir casa de banho, dizíamos-lhe onde estava o dinheiro e o que e que tinham para comprar e nos trazer. Eu nada tenho a queixar-me deles.</p>', NULL, NULL, NULL, 1),
(46, 'Caramó Sanhá', 'Guinieense', '1962-64', '<p>(…) nunca saí fora do campo. De manhã abriram-nos às portas, tomávamos café, depois éramos recolhidos no pavilhão. Havia uma parte dos guineenses e outra dos cabo-verdianos. O máximo que conseguíamos era trocar impressões através de bilhetes. Nas cartas que recebíamos, quando se queria, escrevia-se numa das nossas línguas ou então através de códigos que os portugueses não entendiam.</p>', NULL, NULL, NULL, 1),
(47, 'Mário Soares', 'Guinieense', '1962-1969', '<p>Na altura, éramos chamados e nos perguntavam se estávamos arrependidos. No meu caso, eu respondia que não estava arrependido porque eu não sabia do que era acusado. Por causa disso, achavam que nós ainda tínhamos a ideia de sermos “terroristas”. É isto que os leva a manter-me no Tarrafal até 1969.</p>', NULL, NULL, NULL, 1),
(48, 'Augusto Pereira da Graça', 'Guinieense', '1962-69', '<p>Quando as pessoas ouviram a nossa convocatória pela rádio, por causa do simposium sobre o Tarrafal, ficaram admiradas. “Todos esses nomes é gente que esteve no Tarrafal?!” E quando nos encontravam perguntavam…</p>', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `foto_capa` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto_perfil` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pais` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ilha` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_me` longtext COLLATE utf8_unicode_ci,
  `descricao` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `foto_capa`, `foto_perfil`, `username`, `first_name`, `last_name`, `cidade`, `pais`, `ilha`, `about_me`, `descricao`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'uploads/background.jpg', 'uploads/face.jpg', 'hferreira', 'Helcio', 'Ferreira', 'Praia', 'Cabo Verde', 'Santiago', '<p>Oh so, your weak rhymeYou doubt I\'ll bother, reading into it</p><p>I\'ll probably won\'t, left to my own devices</p><p>But that\'s the difference in our opinions.</p>', 'I like the way you work it No diggity I wanna bag it up', 'OozWeMaaxuqLWdjJoCBTodpihepxw0ah', '$2y$13$1rWW0iWtjL4qtwwMszCfUuG0gwGqSEm4fWg0s/l53j3PamVW5KkLO', 'YOpfxZr1QYnZ9KRt7VKvEfmXUyWu7f7w_1540003025', 'helciotj10@gmail.com', 10, 1511181961, 1540003025),
(2, NULL, NULL, 'Hercules', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PJouilW6pqjnOCD9r4NJwKbcjo6wp9lb', '$2y$13$f51cQZ3S9lV65RnShkMQTeO.7IxwCIZPSTld02fmMb5kUJ8Ik7TU.', 'h5K2FSPJvWei_yh9uxGeuyXUQotBEA3H_1511283947', 'herculesbarber@gmail.com', 10, 1511202515, 1511283947),
(3, 'uploads/18056256_1306517486069951_373948050934832899_o.jpg', 'uploads/23660648_1477941235608317_2082131298_o.jpg', 'sulasian', 'Suzilene', 'Andrade', 'Praia', 'Cabo Verde', 'Santiago', '', 'Presa em meu mundo, escrever, Liberta-me', 'qWsqHEdArBLrfjCiPBJVaJkNckIlPUpu', '$2y$13$YlOejdP8wmukgQLEhqMrvOvUjF3xy5A4q6ellSGczKtY8XXYlptee', NULL, 'suzilenea@gmail.com', 10, 1515780749, 1515780749),
(7, NULL, 'uploads/aa17ea5950f7fc1531c78a0ccb34ef36.png', 'helcio', 'Helcio', 'Ferreira', NULL, NULL, NULL, NULL, NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, 'helciotj10@gmail.com', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `visita`
--

CREATE TABLE `visita` (
  `id` int(11) NOT NULL,
  `video` varchar(500) DEFAULT NULL,
  `foto` varchar(500) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doacao`
--
ALTER TABLE `doacao`
  ADD PRIMARY KEY (`idDoacao`);

--
-- Indexes for table `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`idHistoria`);

--
-- Indexes for table `infoapp`
--
ALTER TABLE `infoapp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infocampo`
--
ALTER TABLE `infocampo`
  ADD PRIMARY KEY (`idInfo`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `midia`
--
ALTER TABLE `midia`
  ADD PRIMARY KEY (`idMidia`),
  ADD KEY `fk_midia_historia_idx1` (`idHistoria`),
  ADD KEY `fk_midia_testemunho_idx1` (`idTestemunho`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `pontosinterece`
--
ALTER TABLE `pontosinterece`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testemunho`
--
ALTER TABLE `testemunho`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `email` (`email`) USING BTREE;

--
-- Indexes for table `visita`
--
ALTER TABLE `visita`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `doacao`
--
ALTER TABLE `doacao`
  MODIFY `idDoacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `historia`
--
ALTER TABLE `historia`
  MODIFY `idHistoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `infoapp`
--
ALTER TABLE `infoapp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `infocampo`
--
ALTER TABLE `infocampo`
  MODIFY `idInfo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `midia`
--
ALTER TABLE `midia`
  MODIFY `idMidia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `pontosinterece`
--
ALTER TABLE `pontosinterece`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `publicacoes`
--
ALTER TABLE `publicacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `testemunho`
--
ALTER TABLE `testemunho`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `visita`
--
ALTER TABLE `visita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
