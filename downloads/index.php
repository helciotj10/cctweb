<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App CCT</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/user.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="#"><i class="glyphicon glyphicon-phone"></i>App CCT</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-right"></ul>
            </div>
        </div>
    </nav>
    <div class="jumbotron hero">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-push-7 phone-preview">
                    <div class="iphone-mockup"><img src="assets/img/iphone.svg" class="device">
                        <div class="screen"><img class="img-responsive" src="assets/img/screen-content-iphone-6.png"></div>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-3 get-it">
                    <h1>App Museu da Resistência</h1>
                    <p>O app do Museu da resisência / Ex Campo de Consentração de Cabo verde, permitirá viver a historia.</p>
                    <p><a class="btn btn-success btn-lg" role="button" href="https://www.cctvirtual.cf/downloads/download.php"><i class="fa fa-google"></i> Disponivel no Google Play</a>
                        <a class="btn btn-primary btn-lg" role="button" href=""><i class="fa fa-apple"></i> Disponivel no App Store</a>
                    </p><img src="assets/img/QR_Code_CCT.png" width="150px"></div>
            </div>
        </div>
    </div>
    <section class="testimonials">
        <h2 class="text-center">Pessoas a Amam!</h2>
        <blockquote>
            <p>Este app me deixou sair, levar os Olhos, a Boca, o Sorriso e o Corpo. Dzem k’ma sim!</p>
            <footer>Suzilene Andrade</footer>
        </blockquote>
    </section>
    <section class="features">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Fantasticas Funcionalidades</h2>
                    <p>Com esta aplicação poderás faser, visitas virtuais pelo ex campo de consentração, conhecer a sua historia, ver testemhunhos de pessoas que passaram por lá, partilhar experiencias, etc.</p>
                </div>
                <div class="col-md-6">
                    <div class="row icon-features">
                        <div class="col-xs-4 icon-feature"><i class="glyphicon glyphicon-eye-open"></i>
                            <p>Visite-nos</p>
                        </div>
                        <div class="col-xs-4 icon-feature"><i class="glyphicon glyphicon-education"></i>
                            <p>Conheça nossa historia</p>
                        </div>
                        <div class="col-xs-4 icon-feature"><i class="glyphicon glyphicon-comment"></i>
                            <p>Partilhe experiencias</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h5>App CCT© 2017</h5></div>
                <div class="col-sm-6 social-icons"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
            </div>
        </div>
    </footer>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>