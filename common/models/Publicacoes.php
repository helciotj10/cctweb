<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "publicacoes".
 *
 * @property int $id
 * @property int $idUser
 * @property string $foto
 * @property string $descricao
 * @property int $gosto
 * @property int $comentario
 * @property string $dia
 * @property string $mes
 * @property string $ano
 * @property int $estado
 */
class Publicacoes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacoes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUser', 'descricao', 'gosto', 'estado'], 'required'],
            [['idUser', 'gosto', 'comentario', 'estado'], 'integer'],
            [['foto'], 'string'],
            [['descricao'], 'string', 'max' => 500],
            [['dia', 'mes', 'ano'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idUser' => 'Id User',
            'foto' => 'Foto',
            'descricao' => 'Descricao',
            'gosto' => 'Gosto',
            'comentario' => 'Comentario',
            'dia' => 'Dia',
            'mes' => 'Mes',
            'ano' => 'Ano',
            'estado' => 'Estado',
        ];
    }
}
