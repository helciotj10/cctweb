<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Infocampo;

/**
 * InfocampoSearch represents the model behind the search form about `common\models\Infocampo`.
 */
class InfocampoSearch extends Infocampo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInfo'], 'integer'],
            [['horaInicio', 'horaFim', 'telefone', 'bilhete_nacional', 'bilhete_internacional', 'horaInicio_FS', 'horaFim_FS', 'endereco', 'extras'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Infocampo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idInfo' => $this->idInfo,
        ]);

        $query->andFilterWhere(['like', 'horaInicio', $this->horaInicio])
            ->andFilterWhere(['like', 'horaFim', $this->horaFim])
            ->andFilterWhere(['like', 'telefone', $this->telefone])
            ->andFilterWhere(['like', 'bilhete_nacional', $this->bilhete_nacional])
            ->andFilterWhere(['like', 'bilhete_internacional', $this->bilhete_internacional])
            ->andFilterWhere(['like', 'horaInicio_FS', $this->horaInicio_FS])
            ->andFilterWhere(['like', 'horaFim_FS', $this->horaFim_FS])
            ->andFilterWhere(['like', 'endereco', $this->endereco])
            ->andFilterWhere(['like', 'extras', $this->extras]);

        return $dataProvider;
    }
}
