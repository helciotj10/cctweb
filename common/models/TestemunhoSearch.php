<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Testemunho;

/**
 * TestemunhoSearch represents the model behind the search form of `common\models\Testemunho`.
 */
class TestemunhoSearch extends Testemunho
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado'], 'integer'],
            [['nome', 'nacionalidade', 'data_nascimento', 'biografiaPT', 'biografiaEN', 'biografiaFR', 'biografiaES'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Testemunho::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'nacionalidade', $this->nacionalidade])
            ->andFilterWhere(['like', 'data_nascimento', $this->data_nascimento])
            ->andFilterWhere(['like', 'biografiaPT', $this->biografiaPT])
            ->andFilterWhere(['like', 'biografiaEN', $this->biografiaEN])
            ->andFilterWhere(['like', 'biografiaFR', $this->biografiaFR])
            ->andFilterWhere(['like', 'biografiaES', $this->biografiaES]);

        return $dataProvider;
    }
}
