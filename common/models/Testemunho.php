<?php

namespace common\models;

use Yii;
use common\models\Midia;
use common\models\MidiaSearch;

/**
 * This is the model class for table "testemunho".
 *
 * @property int $id
 * @property string $nome
 * @property string $nacionalidade
 * @property string $data_nascimento
 * @property string $biografiaPT
 * @property string $biografiaEN
 * @property string $biografiaFR
 * @property string $biografiaES
 * @property int $estado
 */
class Testemunho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testemunho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['biografiaPT', 'biografiaEN', 'biografiaFR', 'biografiaES'], 'string'],
            [['estado'], 'integer'],
            [['nome'], 'string', 'max' => 45],
            [['nacionalidade'], 'string', 'max' => 100],
            [['data_nascimento'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'nacionalidade' => 'Nacionalidade',
            'data_nascimento' => 'Preso de',
            'biografiaPT' => 'Biografia Pt',
            'biografiaEN' => 'Biografia En',
            'biografiaFR' => 'Biografia Fr',
            'biografiaES' => 'Biografia Es',
            'estado' => 'Estado',
        ];
    }

    public function getFotoTestemunho(){
        return $this->hasOne(Midia::className(), ['idTestemunho' => 'id']);
    }
}
