<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "infoapp".
 *
 * @property int $id
 * @property int $dowload
 * @property int $dia
 * @property int $mes
 * @property int $ano
 * @property int $login
 */
class Infoapp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infoapp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dowload', 'dia', 'mes', 'ano', 'login'], 'required'],
            [['dowload', 'dia', 'mes', 'ano', 'login'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dowload' => 'Dowload',
            'dia' => 'Dia',
            'mes' => 'Mes',
            'ano' => 'Ano',
            'login' => 'Login',
        ];
    }
}
