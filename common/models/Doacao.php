<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "doacao".
 *
 * @property int $idDoacao
 * @property string $nome
 * @property string $valor
 * @property string $data
 */
class Doacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['nome', 'valor'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDoacao' => 'Id Doacao',
            'nome' => 'Nome',
            'valor' => 'Valor',
            'data' => 'Data',
        ];
    }
}
