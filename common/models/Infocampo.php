<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "infocampo".
 *
 * @property integer $idInfo
 * @property string $horaInicio
 * @property string $horaFim
 * @property string $telefone
 * @property string $bilhete_nacional
 * @property string $bilhete_internacional
 * @property string $horaInicio_FS
 * @property string $horaFim_FS
 * @property string $endereco
 * @property string $extras
 */
class Infocampo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infocampo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['extras'], 'string'],
            [['horaInicio', 'horaFim'], 'string', 'max' => 5],
            [['telefone', 'bilhete_nacional', 'bilhete_internacional'], 'string', 'max' => 10],
            [['horaInicio_FS', 'horaFim_FS', 'endereco'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInfo' => 'Id Info',
            'horaInicio' => 'Hora de Abertura (para visita)',
            'horaFim' => 'Hora de Fecho (para visita)',
            'telefone' => 'Telefone',
            'bilhete_nacional' => 'Bilhete Nacional',
            'bilhete_internacional' => 'Bilhete Internacional',
            'horaInicio_FS' => 'Hora de Abertura Finais de Semana (para visita)',
            'horaFim_FS' => 'Hora de Fecho Finais de Semana (para visita)',
            'endereco' => 'Endereço Eletronico',
            'extras' => 'Informações Extras',
        ];
    }
}
