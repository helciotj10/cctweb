<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Midia;

/**
 * MidiaSearch represents the model behind the search form of `common\models\Midia`.
 */
class MidiaSearch extends Midia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idMidia', 'estado', 'idHistoria', 'idTestemunho'], 'integer'],
            [['video', 'foto', 'descricaoPT', 'descricaoEN', 'descricaoFR', 'descricaoES'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Midia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idMidia' => $this->idMidia,
            'estado' => $this->estado,
            'idHistoria' => $this->idHistoria,
            'idTestemunho' => $this->idTestemunho,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'descricaoPT', $this->descricaoPT])
            ->andFilterWhere(['like', 'descricaoEN', $this->descricaoEN])
            ->andFilterWhere(['like', 'descricaoFR', $this->descricaoFR])
            ->andFilterWhere(['like', 'descricaoES', $this->descricaoES]);

        return $dataProvider;
    }
}
