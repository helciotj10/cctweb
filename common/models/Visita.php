<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "visita".
 *
 * @property integer $id
 * @property string $video
 * @property string $foto
 * @property string $descricao
 * @property integer $estado
 */
class Visita extends \yii\db\ActiveRecord
{
        public $fotoc;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visita';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado'], 'required'],
            [['estado'], 'integer'],
            [['fotoc'], 'file'],
            [['video', 'foto', 'descricao'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video' => 'Video Inserir codigo do video do youtube. (Ex:QGJuMBdaqIw , Url completo: "https://www.youtube.com/watch?v=QGJuMBdaqIw")',
            'fotoc' => 'Foto',
            'descricao' => 'Breve descrição da midia.',
            'estado' => 'Estado',
        ];
    }
}
