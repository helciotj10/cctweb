<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "historia".
 *
 * @property integer $idHistoria
 * @property string $tituloPT
 * @property string $tituloEN
 * @property string $tituloFR
 * @property string $tituloES
 * @property string $historiaPT
 * @property string $historiaEN
 * @property string $historiaFR
 * @property string $historiaES
 * @property integer $estado
 */
class Historia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['historiaPT', 'historiaEN', 'historiaFR', 'historiaES'], 'string'],
            [['estado'], 'integer'],
            [['tituloPT', 'tituloEN', 'tituloFR', 'tituloES'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idHistoria' => 'Id Historia',
            'tituloPT' => 'Titulo',
            'tituloEN' => 'Titulo En',
            'tituloFR' => 'Titulo Fr',
            'tituloES' => 'Titulo Es',
            'historiaPT' => 'Historia Pt',
            'historiaEN' => 'Historia En',
            'historiaFR' => 'Historia Fr',
            'historiaES' => 'Historia Es',
            'estado' => 'Estado',
        ];
    }
}
