<?php

namespace common\models;
use common\models\Historia;
use common\models\HistoriaSearch;

use Yii;

/**
 * This is the model class for table "midia".
 *
 * @property int $idMidia
 * @property string $video
 * @property string $foto
 * @property string $descricaoPT
 * @property string $descricaoEN
 * @property string $descricaoFR
 * @property string $descricaoES
 * @property int $estado
 * @property int $idHistoria
 * @property int $idTestemunho
 */
class Midia extends \yii\db\ActiveRecord
{
    public $fotoc;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'midia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descricaoPT', 'descricaoEN', 'descricaoFR', 'descricaoES'], 'string'],
            [['estado'], 'required'],
            [['fotoc'], 'file'],
            [['estado', 'tipo', 'idHistoria', 'idTestemunho', 'idPontos'], 'integer'],
            [['video'], 'string', 'max' => 500],
            [['foto'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMidia' => 'Id Midia',
            'tipo' => 'Foto do Quê?',
            'video' => 'Video Inserir codigo do video do youtube. (Ex:QGJuMBdaqIw , Url completo: "https://www.youtube.com/watch?v=QGJuMBdaqIw")',
            'fotoc' => 'Foto',
            'descricaoPT' => 'Breve descrição',
            'descricaoEN' => 'Descricao En',
            'descricaoFR' => 'Descricao Fr',
            'descricaoES' => 'Descricao Es',
            'estado' => 'Estado',
            'idHistoria' => 'Historia',
            'idTestemunho' => 'Testemunho',
            'idPontos' => 'Ponto de Interesse',
        ];
    }

    public function getIdHistoria(){
        return $this->hasOne(Historia::className(), ['idHistoria' => 'idHistoria']);
    }
}
