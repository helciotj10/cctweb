<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Historia;

/**
 * HistoriaSearch represents the model behind the search form about `common\models\Historia`.
 */
class HistoriaSearch extends Historia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idHistoria', 'estado'], 'integer'],
            [['tituloPT', 'tituloEN', 'tituloFR', 'tituloES', 'historiaPT', 'historiaEN', 'historiaFR', 'historiaES'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Historia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idHistoria' => $this->idHistoria,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'tituloPT', $this->tituloPT])
            ->andFilterWhere(['like', 'tituloEN', $this->tituloEN])
            ->andFilterWhere(['like', 'tituloFR', $this->tituloFR])
            ->andFilterWhere(['like', 'tituloES', $this->tituloES])
            ->andFilterWhere(['like', 'historiaPT', $this->historiaPT])
            ->andFilterWhere(['like', 'historiaEN', $this->historiaEN])
            ->andFilterWhere(['like', 'historiaFR', $this->historiaFR])
            ->andFilterWhere(['like', 'historiaES', $this->historiaES]);

        return $dataProvider;
    }
}
