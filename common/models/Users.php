<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $foto_capa
 * @property string $foto_perfil
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $cidade
 * @property string $pais
 * @property string $ilha
 * @property string $about_me
 * @property string $descricao
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Users extends \yii\db\ActiveRecord
{

    public $fotoc;
    public $fotop;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['about_me'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['foto_capa', 'foto_perfil', 'first_name', 'last_name'], 'string', 'max' => 500],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['cidade', 'pais', 'ilha'], 'string', 'max' => 50],
            [['descricao'], 'string', 'max' => 800],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['fotoc', 'fotop'], 'file'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fotoc' => 'Foto da Capa',
            'fotop' => 'Foto do Perfil',
            'username' => 'Username',
            'first_name' => 'Primeiro Nome',
            'last_name' => 'Ultimo Nome',
            'cidade' => 'Cidade',
            'pais' => 'Pais',
            'ilha' => 'Ilha',
            'about_me' => 'Mais Sobre mim',
            'descricao' => 'Drescreva voçê mesmo em uma frase',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
