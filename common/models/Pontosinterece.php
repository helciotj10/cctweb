<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pontosinterece".
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property string $latude
 * @property string $longitude
 */
class Pontosinterece extends \yii\db\ActiveRecord
{
    public $localisacao;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pontosinterece';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descricao'], 'string'],
            [['nome', 'localisacao'], 'string', 'max' => 500],
            [['nome'], 'required', 'message' => 'Campo não pode ser vazio!'],
            [['longitude', 'latude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
            'latude' => 'Latude',
            'longitude' => 'Longitude',
        ];
    }
}
